/*
 * MIT License
 *
 * Copyright (c) 2018 David Antliff
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/**
 * @file app_main.c
 * @brief Example application for the LCD1602 16x2 Character Dot Matrix LCD display via I2C backpack..
 */

#include <stdio.h>
#include <time.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "freertos/timers.h"
#include "freertos/semphr.h"
#include "esp_system.h"
#include "sdcard_list.h"
#include "driver/gpio.h"
#include "driver/i2c.h"
#include "esp_log.h"
#include "smbus.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "esp_wifi.h"
#include "periph_wifi.h"
#include "board.h"
#include "audio_common.h"
#include "audio_pipeline.h"
#include "i2s_stream.h"
#include "raw_stream.h"
#include "looper_radio.h"

/*own libs*/
#include "lcd_menu.h"
#include "looper_sntp.h"
#include "wifi.h"
#include "looper_audio_player.h"
#include "looper_frequency_analysis.h"
#include "rotairy_driver.h"
#include "gpio_board_driver.h"

#include "clock.h"

#define TAG "main"

#define I2C_MASTER_NUM I2C_NUM_0
#define I2C_MASTER_TX_BUF_LEN 0 // disabled
#define I2C_MASTER_RX_BUF_LEN 0 // disabled
#define I2C_MASTER_FREQ_HZ 100000
#define I2C_MASTER_SDA_IO 18
#define I2C_MASTER_SCL_IO 23

#define VOLUME_STEP 10

static uint8_t BPM = 1;

SemaphoreHandle_t general_i2c_xMutex;
rotairy_encoder_info_t *rotairy_encoder_info;
mcp23x17_t *gpio_board_info;
QueueHandle_t looper_audio_player_queue;
audio_board_handle_t board_handle;
SemaphoreHandle_t audio_output_mutex;

static int player_volume;

static void i2c_master_init(void)
{
    int i2c_master_port = I2C_MASTER_NUM;
    i2c_config_t conf;
    conf.mode = I2C_MODE_MASTER;
    conf.sda_io_num = I2C_MASTER_SDA_IO;
    conf.sda_pullup_en = GPIO_PULLUP_DISABLE; // GY-2561 provides 10kΩ pullups
    conf.scl_io_num = I2C_MASTER_SCL_IO;
    conf.scl_pullup_en = GPIO_PULLUP_DISABLE; // GY-2561 provides 10kΩ pullups
    conf.master.clk_speed = I2C_MASTER_FREQ_HZ;
    i2c_param_config(i2c_master_port, &conf);
    i2c_driver_install(i2c_master_port, conf.mode,
                       I2C_MASTER_RX_BUF_LEN,
                       I2C_MASTER_TX_BUF_LEN, 0);
    i2c_set_timeout(I2C_NUM_0, 20000);

    // OUR GENERAL MUTUAL EXCLUSION FOR I2C SAFETY.
    general_i2c_xMutex = xSemaphoreCreateMutex();
}

static void gpio_interrupt(uint8_t pin, uint8_t event, uint8_t reg);

static esp_err_t init_gpio_board_info()
{
    smbus_info_t *smbus_info = smbus_malloc();
    // fill our smbus info with addresses and master port.
    esp_err_t error = smbus_init(smbus_info, I2C_MASTER_NUM, MCP23X17_ADDR_BASE);

    if(error == ESP_FAIL){
        return error;
    }

    gpio_board_info = mcp23x17_info_malloc();
    gpio_board_info->general_i2c_xMutex = &general_i2c_xMutex;
    gpio_board_info->smbus_info = smbus_info;

    mcp23x17_write_reg(gpio_board_info, REG_GPIOB, 0x00);

    // REMOVED BECAUSE OF MEMORY SHORTAGE
    // gpio_board_interrupt_cfg_t *cgf = gpio_board_interrupt_cfg_malloc();
    // cgf->cfg_reg_a = 0b00010001;
    // cgf->cfg_reg_b = 0b00000000;
    // cgf->event_trigger_reg_a = 0b00000001;
    // cgf->event_trigger_reg_b = 0b00000000;
    // cgf->gpio_board_info = gpio_board_info;
    // cgf->event = &gpio_interrupt;
    // gpio_board_driver_start_task(cgf);

    return error;
}

static void onButtonCLicked()
{
    //ESP_LOGI(TAG, "onbuttonClicked");
    click_and_draw();

    uint8_t r;
    uint8_t g;
    uint8_t b;
    pass_current_menu_color(&r, &g, &b);
    rotairy_driver_set_color(rotairy_encoder_info, r, g, b);
}

static void onButtonPressed()
{
    //ESP_LOGI(TAG, "onbuttonPressed");
}

static void onRotairyTurned(int16_t amount)
{
    //ESP_LOGI(TAG, "onRotairyTurned result: %d", amount);
    scroll_and_draw(amount);

    uint8_t r;
    uint8_t g;
    uint8_t b;
    pass_current_menu_color(&r, &g, &b);
    // ESP_LOGI(TAG, "ROTARY TURNED colors %d %d %d", r, g, b);
    rotairy_driver_set_color(rotairy_encoder_info, r, g, b);
}

// REMOVED BECAUSE OF MEMORY SHORTAGE
// static void gpio_interrupt(uint8_t pin, uint8_t event, uint8_t reg)
// {
//     ESP_LOGI(TAG, "interrupt on pin: %d, rising or falling: %d, register: %d", pin, event, reg);
// }

esp_err_t init_rotairy_encoder()
{

    // allocate space for our smbus info.
    smbus_info_t *rotairy_smbus_info = smbus_malloc();
    // fill our smbus info with addresses and master port.
    esp_err_t error = smbus_init(rotairy_smbus_info, I2C_MASTER_NUM, ROTAIRY_I2C_ADDRESS);
    error = smbus_set_timeout(rotairy_smbus_info, 1000 / portTICK_RATE_MS);

    rotairy_encoder_info = rotairy_info_malloc();
    rotairy_encoder_info->smbus_info = rotairy_smbus_info;
    rotairy_encoder_info->general_i2c_xMutex = &general_i2c_xMutex;

    rotairy_driver_init(onRotairyTurned, onButtonCLicked, onButtonPressed);
    rotairy_driver_start_task(rotairy_encoder_info);

    // to stop the task: rotairy_driver_stop_task();

    return error;
}

void nvs_init()
{
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND)
    {
        ESP_ERROR_CHECK(nvs_flash_erase());
        ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);
}

void menu_on_change_BPM(uint16_t amount, uint8_t *out)
{

    BPM += amount;

    if (BPM > 200)
    {
        BPM = 200;
    }
    else if (BPM < 1)
    {
        BPM = 1;
    }

    *out = BPM;
}

void menu_metronome_start(void)
{
    if (gpio_board_driver_start_metronome(gpio_board_info, BPM) == ESP_OK)
    {
        return;
    }
    gpio_board_driver_stop_metronome();
    gpio_board_driver_start_metronome(gpio_board_info, BPM);
}

void menu_metronome_stop(void)
{
    gpio_board_driver_stop_metronome();
}

// callback method that gets called when the "play" looper menu option is called in the menu
void menu_on_play_pressed_callback(void)
{
    int data = LOOPER_AUDIO_AUD;
    xQueueSend(looper_audio_player_queue, &data , portMAX_DELAY);
    looper_audio_player_play();
}

void menu_on_stop_pressed_callback(void)
{
    looper_audio_player_pause();
}

void menu_volume_up_callback(void)
{
    audio_hal_get_volume(board_handle->audio_hal, &player_volume);

    //ESP_LOGI(TAG, "[ * ] [Vol+] rotairy event");
    player_volume += VOLUME_STEP;
    if (player_volume > 100)
    {
        player_volume = 100;
    }
    audio_hal_set_volume(board_handle->audio_hal, player_volume);
    // ESP_LOGI(TAG, "[ * ] Volume set to %d %%", player_volume);
}

void menu_volume_down_callback(void)
{
    audio_hal_get_volume(board_handle->audio_hal, &player_volume);

    // ESP_LOGI(TAG, "[ * ] [Vol-] rotairy event");
    player_volume -= VOLUME_STEP;
    if (player_volume < 0)
    {
        player_volume = 0;
    }
    audio_hal_set_volume(board_handle->audio_hal, player_volume);
    // ESP_LOGI(TAG, "[ * ] Volume set to %d %%", player_volume);
}

void menu_station_up_callback(void)
{
    looper_radio_up();
}

void menu_station_down_callback(void)
{
    looper_radio_down();
}

void menu_on_tell_time_callback(void)
{
    clock_set_minute_on_queue();
    looper_audio_player_play();
}

void menu_on_radio_stop(void)
{
    looper_radio_stop();
}

void menu_on_radio_play(void)
{
    looper_radio_play();
}

void goertzel_callback(void)
{

    //ESP_LOGI(TAG, "goertzel");
    clock_set_minute_on_queue();
    looper_audio_player_play();
    setup_goertzel();
}

void start_goertzel()
{

    frequency_analysis_set_on_goertzel_callback(&goertzel_callback);
    frequency_analysis_start_listening();
}

void timer(xTimerHandle xTimer)
{
    ESP_LOGI(TAG, "free heap %d", xPortGetFreeHeapSize());
}

void app_main()
{
    ESP_LOGI(TAG, "start free heap %d", xPortGetFreeHeapSize());

    looper_audio_player_queue = xQueueCreate(25, sizeof(int));
    audio_output_mutex = xSemaphoreCreateCounting(1, 1);

    nvs_init();

    //initiate resources
    esp_periph_config_t periph_cfg = DEFAULT_ESP_PERIPH_SET_CONFIG();
    esp_periph_set_handle_t set = esp_periph_set_init(&periph_cfg);

    audio_board_sdcard_init(set, SD_MODE_1_LINE);

    board_handle = audio_board_init();
    audio_hal_ctrl_codec(board_handle->audio_hal, AUDIO_HAL_CODEC_MODE_DECODE, AUDIO_HAL_CTRL_START);

    i2c_master_init();
    init_rotairy_encoder();
    init_gpio_board_info();
    init_LCD_menu(&general_i2c_xMutex);

    looper_audio_player_init(set, looper_audio_player_queue, audio_output_mutex);

    // set the menu play callback to our callback method
    set_menu_play_callback(&menu_on_play_pressed_callback);
    set_menu_stop_callback(&menu_on_stop_pressed_callback);
    set_menu_volume_up_callback(&menu_volume_up_callback);
    set_menu_volume_down_callback(&menu_volume_down_callback);
    set_menu_station_up_callback(&menu_station_up_callback);
    set_menu_station_down_callback(&menu_station_down_callback);
    set_menu_tell_time_callback(&menu_on_tell_time_callback);
    set_menu_metronome_on_change_BPM(&menu_on_change_BPM);
    set_menu_metronome_start(&menu_metronome_start);
    set_menu_metronome_stop(&menu_metronome_stop);
    set_menu_radio_stop_callback(&menu_on_radio_stop);
    set_menu_radio_play_callback(&menu_on_radio_play);

    //ESP_LOGI(TAG, "ESP_WIFI_MODE_STA");
    init_wifi();

    sntp_sync(NULL);

    start_goertzel();

    clock_init(looper_audio_player_queue);

    TimerHandle_t timerHandle = xTimerCreate("timer", pdMS_TO_TICKS(500), pdTRUE, (void *)0, &timer);
    ESP_LOGI(TAG, "timer start %x", xTimerStart(timerHandle, 0));
    looper_radio_init(set, audio_output_mutex);
}
