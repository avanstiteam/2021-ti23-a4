#pragma once
#ifndef LOOPER_SNTP_H
#define LOOPER_SNTP_H
#include "esp_sntp.h"
#include "esp_err.h"


esp_err_t sntp_sync(sntp_sync_time_cb_t callback);
esp_err_t get_status(void);

#endif
