#include "looper_sntp.h"
#include <time.h>
#include "esp_log.h"

static const char *TAG = "SNTP";

static int obtain_time(void);
static void initialize_sntp(void);

void empty_cb(struct timeval *tv)
{
}
/*
blocking call
*/
esp_err_t sntp_sync(sntp_sync_time_cb_t callback)
{
    sntp_set_time_sync_notification_cb((callback == NULL) ? empty_cb : callback); //not sure if NULL will break

    // Set callback for when time synchronisation is done
    // Set timezone to Asmterdam Time
    setenv("TZ", "CET-1", 1);
    tzset();

    time_t now;
    struct tm timeinfo;
    time(&now);
    localtime_r(&now, &timeinfo);

    esp_err_t error = ESP_OK;
    // Is time set? If not, tm_year will be (1970 - 1900).
    if (timeinfo.tm_year < (2016 - 1900))
    {
        // ESP_LOGI(TAG, "Time is not set yet. Connecting to WiFi and getting time over NTP.");
        error = obtain_time();
        // update 'now' variable with current time
        time(&now);
    }

    localtime_r(&now, &timeinfo);

    // ESP_LOGI(TAG, "time %02d:%02d:%02d\n", timeinfo.tm_hour, timeinfo.tm_min, timeinfo.tm_sec);

    return error;
}

esp_err_t get_status(void)
{
    return (sntp_get_sync_status() == SNTP_SYNC_STATUS_COMPLETED) ? ESP_OK : ESP_ERR_TIMEOUT;
}

// Obtain SNTP time
static esp_err_t obtain_time(void)
{
    initialize_sntp();

    // wait for time to be set
    int retry = 0;
    const int retry_count = 10;
    while (sntp_get_sync_status() == SNTP_SYNC_STATUS_RESET && ++retry < retry_count)
    {
        ESP_LOGI(TAG, "Waiting for system time to be set... (%d/%d)", retry, retry_count);
        vTaskDelay(2000 / portTICK_PERIOD_MS);
    }
    return (sntp_get_sync_status() == SNTP_SYNC_STATUS_COMPLETED) ? ESP_OK : ESP_ERR_TIMEOUT;
}

// Set synchronisation settings
static void initialize_sntp(void)
{
    // ESP_LOGI(TAG, "Initializing SNTP");
    sntp_setoperatingmode(SNTP_OPMODE_POLL);
    sntp_setservername(0, "pool.ntp.org");

    sntp_init();
}