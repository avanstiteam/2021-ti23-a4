# https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-guides/build-system.html#example-component-requirements

idf_component_register(SRCS "i2c-lcd1602.c"                 # SRC tells cmake which .c files we want to include
                        INCLUDE_DIRS "include"                    # INCLUDE_DIRS gives the list of public include directories for this component 
                        REQUIRES s_smbus)                   # REQUIRES gives the list of components required by the public interface of this component.