#pragma once
#ifndef CLOCK_H
#define CLOCK_H
#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"

/**
 * @brief initializes the clock and updates the time and date to the lcd
 */
void clock_init(QueueHandle_t queue);

/**
 * @brief gets current time and sets the audio files to play on the queue up to hour accuracy 
 * 
 */
void clock_set_hour_on_queue();

/**
 * @brief gets current time and sets the audio files to play on the queue up to minute accuracy 
 * 
 */
void clock_set_minute_on_queue();

#endif