#include "clock.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/timers.h"

#include <time.h>
#include "esp_log.h"
#include "lcd_menu.h"
#include "looper_audio_player.h"

static const char *TAG = "Clock";
static char month[12][4] = {
    "jan",
    "feb",
    "mar",
    "apr",
    "may",
    "jun",
    "jul",
    "aug",
    "sep",
    "oct",
    "nov",
    "dec",
};

static QueueHandle_t output_queue;

/**
 * @brief updates the time and date to the lcd
 * 
 * @param timeinfo the time info struct that holds the time and date information
 */

static void set_time_on_lcd(struct tm timeinfo);
/**
 * @brief Set the time on the queue up to hour accuracy
 * 
 * @param timeinfo the time info struct that holds the time and date information
 */
static void set_time_hour_on_queue(struct tm timeinfo);

/**
 * @brief Set the time on the queue up to minute accuracy
 * 
 * @param timeinfo the time info struct that holds the time and date information
 */
static void set_time_minute_on_queue(struct tm timeinfo);

/**
 * @brief sets the data on the queue
 * 
 * @param data the date to put on the queue 
 */
static void send_data(int *data);

/**
 * @brief Get the local timeinfo 
 * 
 * @param[out] timeinfo the timeinfo
 */
static void get_timeinfo(struct tm *timeinfo);

static void send_data(int *data)
{
    int ret = xQueueSend(output_queue, data, portMAX_DELAY);
    if (ret != pdTRUE)
    {
        ESP_LOGE(TAG, "Cannot queue data %d", ret);
    }
}

static void get_timeinfo(struct tm *timeinfo)
{
    time_t now;
    time(&now);
    localtime_r(&now, timeinfo);
}

static void set_time_hour_on_queue(struct tm timeinfo)
{

    int data = LOOPER_AUDIO_IT_IS_NOW;
    send_data(&data);

    int hour = timeinfo.tm_hour;
    hour = (hour == 0 ? 12 : hour);
    hour = (hour > 12 ? hour % 12 : hour);
    data = LOOPER_AUDIO_ZERO + hour;
    send_data(&data);

    data = LOOPER_AUDIO_O_CLOCK;
    send_data(&data);
}
static void set_time_minute_on_queue(struct tm timeinfo)
{
    int data = LOOPER_AUDIO_IT_IS_NOW;
    send_data(&data);

    int hour = timeinfo.tm_hour;
    hour = (hour == 0 ? 12 : hour);
    hour = (hour > 12 ? hour % 12 : hour);
    data = LOOPER_AUDIO_ZERO + hour;
    send_data(&data);

    data = LOOPER_AUDIO_O_CLOCK;
    send_data(&data);

    int minute = timeinfo.tm_min;
    data = LOOPER_AUDIO_ZERO + minute;
    send_data(&data);
}

static void set_time_on_lcd(struct tm timeinfo)
{
    char date[7];
    snprintf(date, 7, "%s %02d", month[timeinfo.tm_mon], timeinfo.tm_mday);

    char time[6];
    snprintf(time, 6, "%02d:%02d", timeinfo.tm_hour, timeinfo.tm_min);

    set_date(date);
    set_time(time);
    draw_menu();
}

void clock_set_hour_on_queue()

{
    struct tm timeinfo;
    get_timeinfo(&timeinfo);
    set_time_hour_on_queue(timeinfo);
}
void clock_set_minute_on_queue()
{
    struct tm timeinfo;
    get_timeinfo(&timeinfo);
    set_time_minute_on_queue(timeinfo);
}

/**
 * @brief task that handles the timing for updating the lcd with the time and date. It waits 1 minute before checking the time and date again and updating it on the lcd.
 * 
 * @param pvParameters task parameter. not used.
 */
void xTimerTask(void *pvParameters)
{
    TickType_t xLastWakeTime;
    xLastWakeTime = xTaskGetTickCount();

    while (1)
    {
        time_t now;
        struct tm timeinfo;
        time(&now);
        localtime_r(&now, &timeinfo);
        ESP_LOGI(TAG, "time %02d:%02d:%02d", timeinfo.tm_hour, timeinfo.tm_min, timeinfo.tm_sec);
        set_time_on_lcd(timeinfo);
        if (timeinfo.tm_min == 0)
        {
            clock_set_hour_on_queue(timeinfo);
            looper_audio_player_play();
        }
        // wait exactly 1 minute
        vTaskDelayUntil(&xLastWakeTime, pdMS_TO_TICKS(60000));
    }
}

void clock_init(QueueHandle_t queue)
{
    output_queue = queue;
    struct tm timeinfo;
    get_timeinfo(&timeinfo);
    // set_time_minute_on_queue(timeinfo);
    set_time_on_lcd(timeinfo);
    // looper_audio_player_play();
    BaseType_t ret = xTaskCreate(&xTimerTask, "clock timer task", 2048, NULL, 1, NULL);
    if (ret == pdFAIL)
    {
        // ESP_LOGE(TAG, "ERROR not enough heap memory to create clock timer task.");
    }
}