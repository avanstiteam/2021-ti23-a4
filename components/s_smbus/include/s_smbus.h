#ifndef S_SMBUS_H
#define S_SMBUS_H


#include "esp_system.h"

//#ifndef SMBUS_H
#include "smbus.h"
esp_err_t s_smbus_init();
esp_err_t s_smbus_write_byte(smbus_info_t *info, SemaphoreHandle_t xMutex, uint8_t command, uint8_t data);
esp_err_t s_smbus_read_byte(smbus_info_t *info, SemaphoreHandle_t xMutex, uint8_t command, uint8_t *data);
esp_err_t s_smbus_read_word(smbus_info_t * smbus_info, SemaphoreHandle_t xMutex, uint8_t command, uint16_t * data);
esp_err_t s_smbus_write_word(smbus_info_t * smbus_info, SemaphoreHandle_t xMutex, uint8_t command, uint16_t data);
esp_err_t s_smbus_send_byte(const smbus_info_t *info, SemaphoreHandle_t * xMutex, uint8_t data);

#endif