#include "freertos/FreeRTOS.h"
#include "freertos/semphr.h"
#include "esp_log.h"
#include "driver/i2c.h"

#ifndef SMBUS_H
#include "smbus.h"
#endif

#define TAG "s_smbus"

esp_err_t s_smbus_write_byte(const smbus_info_t *info, SemaphoreHandle_t *xMutex, uint8_t command, uint8_t data)
{
    if (xSemaphoreTake(*xMutex, portMAX_DELAY) == pdTRUE)
    {
        esp_err_t error = smbus_write_byte(info, command, data);
        xSemaphoreGive(*xMutex);
        return error;
    }

    return ESP_ERR_NOT_FOUND;
}

esp_err_t s_smbus_read_byte(const smbus_info_t *info, SemaphoreHandle_t *xMutex, uint8_t command, uint8_t *data)
{
    if (xSemaphoreTake(*xMutex, portMAX_DELAY))
    {
        esp_err_t error = smbus_read_byte(info, command, data);
        xSemaphoreGive(*xMutex);
        return error;
    };
    return ESP_ERR_NOT_FOUND;
}

esp_err_t s_smbus_read_word(const smbus_info_t *smbus_info, SemaphoreHandle_t *xMutex, uint8_t command, uint16_t *data)
{
    if (xSemaphoreTake(*xMutex, portMAX_DELAY) == pdTRUE)
    {
        esp_err_t error = smbus_read_word(smbus_info, command, data);
        xSemaphoreGive(*xMutex);
        return error;
    }

    return ESP_ERR_NOT_FOUND;
}

esp_err_t s_smbus_write_word(const smbus_info_t *smbus_info, SemaphoreHandle_t *xMutex, uint8_t command, uint16_t data)
{
    if (xSemaphoreTake(*xMutex, portMAX_DELAY) == pdTRUE)
    {
        esp_err_t error = smbus_write_word(smbus_info, command, data);
        xSemaphoreGive(*xMutex);
        return error;
    }
    return ESP_ERR_NOT_FOUND;
}

esp_err_t s_smbus_send_byte(const smbus_info_t *info, SemaphoreHandle_t *xMutex, uint8_t data)
{
    if (xSemaphoreTake(*xMutex, portMAX_DELAY) == pdTRUE)
    {
        esp_err_t error = smbus_send_byte(info, data);
        xSemaphoreGive(*xMutex);
        return error;
    }
    return ESP_ERR_NOT_FOUND;
}