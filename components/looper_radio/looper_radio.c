#include "looper_radio.h"
#include "esp_log.h"
#include "audio_pipeline.h"
#include "i2s_stream.h"
#include "aac_decoder.h"
#include "http_stream.h"
#include "mp3_decoder.h"

static const char *TAG = "HTTP_LIVINGSTREAM_EXAMPLE";

#define AAC_STREAM_URI "http://open.ls.qingting.fm/live/274/64k.m3u8?format=aac"
#define mp3_STREAM_URI "https://icecast-qmusicnl-cdp.triple-it.nl/Qmusic_nl_live_96.mp3"
#define NUM_STATIONS 2

typedef struct
{
    char stationName[21];
    char uri[100];
} station_t;

static const station_t stations[NUM_STATIONS] =
    {
        {"Q music",
         "https://icecast-qmusicnl-cdp.triple-it.nl/Qmusic_nl_live_96.mp3"},
        {"Slam",
         "https://stream.slam.nl/web13_mp3"},
};

static audio_pipeline_handle_t pipeline;
static audio_element_handle_t http_stream_reader, i2s_stream_writer, mp3_decoder;
static audio_event_iface_handle_t evt;
static SemaphoreHandle_t pipeline_mutex;
static int station_index;
static SemaphoreHandle_t output_mutex;

static char *getStationUri();

void looper_radio_event_listener_task(void *pvParameters)
{
    while (1)
    {
        audio_event_iface_msg_t msg;
        esp_err_t ret = audio_event_iface_listen(evt, &msg, portMAX_DELAY);
        if (ret != ESP_OK)
        {
            ESP_LOGE(TAG, "[ * ] Event interface error : %d", ret);
            continue;
        }

        if (msg.source_type == AUDIO_ELEMENT_TYPE_ELEMENT && msg.source == (void *)mp3_decoder && msg.cmd == AEL_MSG_CMD_REPORT_MUSIC_INFO)
        {
            audio_element_info_t music_info = {0};
            audio_element_getinfo(mp3_decoder, &music_info);

            // ESP_LOGI(TAG, "[ * ] Receive music info from aac decoder, sample_rates=%d, bits=%d, ch=%d",
            //          music_info.sample_rates, music_info.bits, music_info.channels);

            audio_element_setinfo(i2s_stream_writer, &music_info);
            i2s_stream_set_clk(i2s_stream_writer, music_info.sample_rates, music_info.bits, music_info.channels);
            continue;
        }

        /* restart stream when the first pipeline element (http_stream_reader in this case) receives stop event (caused by reading errors) */
        if (msg.source_type == AUDIO_ELEMENT_TYPE_ELEMENT && msg.source == (void *)http_stream_reader && msg.cmd == AEL_MSG_CMD_REPORT_STATUS && (int)msg.data == AEL_STATUS_ERROR_OPEN)
        {
            ESP_LOGW(TAG, "[ * ] Restart stream");
            audio_pipeline_stop(pipeline);
            audio_pipeline_wait_for_stop(pipeline);
            audio_element_reset_state(mp3_decoder);
            audio_element_reset_state(i2s_stream_writer);
            audio_pipeline_reset_ringbuffer(pipeline);
            audio_pipeline_reset_items_state(pipeline);
            audio_pipeline_run(pipeline);
            continue;
        }
    }
}

static int _http_stream_event_handle(http_stream_event_msg_t *msg)
{
    if (msg->event_id == HTTP_STREAM_RESOLVE_ALL_TRACKS)
    {
        return ESP_OK;
    }

    if (msg->event_id == HTTP_STREAM_FINISH_TRACK)
    {
        return http_stream_next_track(msg->el);
    }
    if (msg->event_id == HTTP_STREAM_FINISH_PLAYLIST)
    {
        return http_stream_fetch_again(msg->el);
    }
    return ESP_OK;
}

esp_err_t looper_radio_init(esp_periph_set_handle_t set, SemaphoreHandle_t audio_output_mutex)
{

    if (pipeline != NULL)
    {
        ESP_LOGW(TAG, "looper_radio already initialized");
        return ESP_FAIL;
    }
    output_mutex = audio_output_mutex;

    //ESP_LOGI(TAG, "[2.0] Create audio pipeline for playback");
    audio_pipeline_cfg_t pipeline_cfg = DEFAULT_AUDIO_PIPELINE_CONFIG();
    pipeline = audio_pipeline_init(&pipeline_cfg);

   // ESP_LOGI(TAG, "[2.1] Create http stream to read data");
    http_stream_cfg_t http_cfg = HTTP_STREAM_CFG_DEFAULT();
    http_cfg.task_core = 1;
    http_cfg.event_handle = _http_stream_event_handle;
    http_cfg.type = AUDIO_STREAM_READER;
    http_cfg.enable_playlist_parser = true;
    http_stream_reader = http_stream_init(&http_cfg);

   // ESP_LOGI(TAG, "[2.2] Create i2s stream to write data to codec chip");
    i2s_stream_cfg_t i2s_cfg = I2S_STREAM_CFG_DEFAULT();
    i2s_cfg.task_core = 1;
    i2s_cfg.type = AUDIO_STREAM_WRITER;
    i2s_stream_writer = i2s_stream_init(&i2s_cfg);

    //ESP_LOGI(TAG, "[2.3] Create mp3 decoder to decode mp3 file");
    mp3_decoder_cfg_t mp3_cfg = DEFAULT_MP3_DECODER_CONFIG();
    mp3_cfg.task_core = 1;
    mp3_decoder = mp3_decoder_init(&mp3_cfg);

    //ESP_LOGI(TAG, "[2.4] Register all elements to audio pipeline");
    audio_pipeline_register(pipeline, http_stream_reader, "http");
    audio_pipeline_register(pipeline, mp3_decoder, "mp3");
    audio_pipeline_register(pipeline, i2s_stream_writer, "i2s");

   // ESP_LOGI(TAG, "[2.5] Link it together http_stream-->mp3_decoder-->i2s_stream-->[codec_chip]");
    const char *link_tag[3] = {"http", "mp3", "i2s"};
    audio_pipeline_link(pipeline, &link_tag[0], 3);
    //audio_pipeline_link(pipeline, (const char *[]) {"http",  "mp3", "i2s"}, 3);

   // ESP_LOGI(TAG, "[2.6] Set up  uri (http as http_stream, mp3 as mp3 decoder, and default output is i2s)");
    audio_element_set_uri(http_stream_reader, mp3_STREAM_URI);

    //ESP_LOGI(TAG, "[ 4 ] Set up  event listener");
    audio_event_iface_cfg_t evt_cfg = AUDIO_EVENT_IFACE_DEFAULT_CFG();
    evt = audio_event_iface_init(&evt_cfg);

    //ESP_LOGI(TAG, "[4.1] Listening event from all elements of pipeline");
    audio_pipeline_set_listener(pipeline, evt);

    //ESP_LOGI(TAG, "[4.2] Listening event from peripherals");
    audio_event_iface_set_listener(esp_periph_set_get_event_iface(set), evt);

    pipeline_mutex = xSemaphoreCreateMutex();

    xTaskCreate(&looper_radio_event_listener_task, "looper radio event listener task", 1024 * 2, NULL, 5, NULL);

    return ESP_OK;
}

void looper_radio_stop()
{
    audio_element_state_t el_state = audio_element_get_state(i2s_stream_writer);
    if (el_state == AEL_STATE_RUNNING)
    {
        if (xSemaphoreTake(pipeline_mutex, portMAX_DELAY) == pdTRUE)
        {
            audio_pipeline_terminate(pipeline);
            audio_pipeline_reset_ringbuffer(pipeline);
            audio_pipeline_reset_elements(pipeline);
            xSemaphoreGive(output_mutex);
            xSemaphoreGive(pipeline_mutex);
        }
    }
}

void looper_radio_play()
{
    audio_element_state_t el_state = audio_element_get_state(i2s_stream_writer);
    //ESP_LOGI(TAG, "el_state play %d", el_state);
    if (el_state == AEL_STATE_INIT)
    {
        if (xSemaphoreTake(pipeline_mutex, portMAX_DELAY) == pdTRUE)
        {
            if (xSemaphoreTake(output_mutex, pdMS_TO_TICKS(100)) == pdTRUE)
            {
                ESP_LOGI(TAG, "playing radio %s", stations[station_index].stationName);
                audio_element_set_uri(http_stream_reader, getStationUri());
                audio_pipeline_reset_ringbuffer(pipeline);
                audio_pipeline_reset_elements(pipeline);
                audio_pipeline_change_state(pipeline, AEL_STATE_INIT);
                audio_pipeline_run(pipeline);
            }
            xSemaphoreGive(pipeline_mutex);
        }
    }
}

void looper_radio_up()
{
    //critical section
    station_index++;
    station_index %= NUM_STATIONS;
}
void looper_radio_down()
{
    //critical section
    station_index--;
    station_index %= NUM_STATIONS;
}

static char *getStationUri()
{
    //critical section
    return stations[station_index].uri;
}
