#pragma once
#ifndef LOOPER_RADIO_H
#define LOOPER_RADIO_H
#include "esp_err.h"
#include "esp_peripherals.h"
#include "freertos/semphr.h"

esp_err_t looper_radio_init(esp_periph_set_handle_t set, SemaphoreHandle_t audio_output_mutex);
void looper_radio_stop();
void looper_radio_play();

void looper_radio_up();
void looper_radio_down();

#endif