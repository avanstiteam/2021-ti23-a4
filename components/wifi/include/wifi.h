#pragma once
#ifndef WIFI_H
#define WIFI_H
#include "esp_err.h"

#define WIFI_CONNECTED 1
#define WIFI_ERROR 0

esp_err_t init_wifi(void);
esp_err_t isConnected();
#endif