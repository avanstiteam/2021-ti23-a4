#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_event.h"
#include "esp_log.h"
#include "esp_attr.h"

#include "rotairy_driver.h"
#include <string.h>

// smbus semaphored.
#include "s_smbus.h"
// smbus
#include "smbus.h"

static const char *TAG = "rotairy_driver";

// task is running or not.
bool task_running;

void (*onButtonClicked)(void);
void (*onButtonPressed)(void);
void (*onRotairyTurned)(int16_t);

// METHOD CALLBACKS HAVE TO BE STATIC SO THEY WON'T BE DISPOSED.
esp_err_t rotairy_driver_init(void (*onRotairyTurned_p)(int16_t), 
								void (*onButtonClicked_p)(void), 
								void (*onButtonPressed_p)(void)){
    
	esp_err_t error = ESP_OK;

    if(onRotairyTurned_p == NULL || onButtonClicked_p == NULL || onButtonPressed_p == NULL){
		// the callbacks have to be NOTNULL.
        return ESP_FAIL;
    }

	// set internal callbacks
    onButtonPressed = onButtonPressed_p;
    onButtonClicked = onButtonClicked_p;
    onRotairyTurned = onRotairyTurned_p;

	// if one of the above functions generated an error.
    return error;
}

rotairy_encoder_info_t * rotairy_info_malloc(void)
{
    rotairy_encoder_info_t * rotairy_info = malloc(sizeof(*rotairy_info));
    if (rotairy_info != NULL)
    {
        memset(rotairy_info, 0, sizeof(*rotairy_info));

        //ESP_LOGD(TAG, "malloc rotairy %p", rotairy_info);
    }
    else
    {
        ESP_LOGE(TAG, "malloc rotairy failed");
    }
    return rotairy_info;
}

esp_err_t rotairy_driver_set_color(rotairy_encoder_info_t * rotairy_encoder_info, uint8_t r, uint8_t g, uint8_t b){
    esp_err_t error = ESP_OK;

    error = s_smbus_write_byte(rotairy_encoder_info->smbus_info, rotairy_encoder_info->general_i2c_xMutex, QWIIC_TWIST_LED_BRIGHTNESS_RED, r);
    // check if it worked.
    if(error != ESP_OK){
        ESP_LOGE(TAG, "can't write to rotairy error: %d", error);
        return error;
    }

    error = s_smbus_write_byte(rotairy_encoder_info->smbus_info, rotairy_encoder_info->general_i2c_xMutex, QWIIC_TWIST_LED_BRIGHTNESS_GREEN, g);
    // check if it worked.
    if(error != ESP_OK){
        ESP_LOGE(TAG, "can't write to rotairy error: %d", error);
        return error;
    }

    error = s_smbus_write_byte(rotairy_encoder_info->smbus_info, rotairy_encoder_info->general_i2c_xMutex, QWIIC_TWIST_LED_BRIGHTNESS_BLUE, b);
    // check if it worked.
    if(error != ESP_OK){
        ESP_LOGE(TAG, "can't write to rotairy error: %d", error);
        return error;
    }

    return error;
}

// Get our current displaying collor.
esp_err_t rotairy_driver_get_color(rotairy_encoder_info_t * rotairy_encoder_info, uint8_t *r, uint8_t *g, uint8_t *b){
    esp_err_t error = ESP_OK;

    error = s_smbus_read_byte(rotairy_encoder_info->smbus_info, rotairy_encoder_info->general_i2c_xMutex, QWIIC_TWIST_LED_BRIGHTNESS_RED, r);
    
	if(error == ESP_FAIL){
		return error;
	}

	error = s_smbus_read_byte(rotairy_encoder_info->smbus_info, rotairy_encoder_info->general_i2c_xMutex, QWIIC_TWIST_LED_BRIGHTNESS_GREEN, g);
    
	if(error == ESP_FAIL){
		return error;
	}
	
	error = s_smbus_read_byte(rotairy_encoder_info->smbus_info, rotairy_encoder_info->general_i2c_xMutex, QWIIC_TWIST_LED_BRIGHTNESS_BLUE, b);

    return error;
}
// Get our current count from encoder.
esp_err_t rotairy_driver_get_count(rotairy_encoder_info_t * rotairy_encoder_info, uint16_t *count){
	esp_err_t error = ESP_OK;
	
	error = s_smbus_read_word(rotairy_encoder_info->smbus_info, rotairy_encoder_info->general_i2c_xMutex, QWIIC_TWIST_ENCODER_COUNT_LSB, (uint16_t *)count);
	
	return error;
}
// Set current count from encoder.
esp_err_t rotairy_driver_set_count(rotairy_encoder_info_t * rotairy_encoder_info, uint16_t count) {
	esp_err_t error = ESP_OK;
	
	error = s_smbus_write_word(rotairy_encoder_info->smbus_info, rotairy_encoder_info->general_i2c_xMutex, QWIIC_TWIST_ENCODER_COUNT_LSB, count);
	
	return error;
}
// Get difference since previous call. If clearvalue is enabled.
esp_err_t rotairy_driver_get_diff(rotairy_encoder_info_t * rotairy_encoder_info, uint16_t *count, bool clearValue) {
	esp_err_t error = ESP_OK;
	
	error =  s_smbus_read_word(rotairy_encoder_info->smbus_info, rotairy_encoder_info->general_i2c_xMutex, QWIIC_TWIST_ENCODER_DIFFERENCE_LSB, (uint16_t *)count);
	
	if(error == ESP_FAIL){
		return error;
	}

	if (clearValue == true) {
		error =  s_smbus_write_word(rotairy_encoder_info->smbus_info, rotairy_encoder_info->general_i2c_xMutex, QWIIC_TWIST_ENCODER_DIFFERENCE_LSB, (uint16_t)0x00);
	}
	
	return error;
}
// Check if the rotairy has moved
esp_err_t rotairy_driver_is_moved(rotairy_encoder_info_t * rotairy_encoder_info, bool* result) {
	esp_err_t error = ESP_OK;
	
	uint8_t status = 0;
	error = s_smbus_read_byte(rotairy_encoder_info->smbus_info, rotairy_encoder_info->general_i2c_xMutex, QWIIC_TWIST_STATUS, &status);
	
	if(error == ESP_FAIL){
		return error;
	}
	
	*result = status & (1 << QWIIC_TWIST_STATUS_MOVED);
	
	error = s_smbus_write_byte(rotairy_encoder_info->smbus_info, rotairy_encoder_info->general_i2c_xMutex, QWIIC_TWIST_STATUS, (status & ~(1 << QWIIC_TWIST_STATUS_MOVED)));  //We've read this status bit, now clear it
	
	return error;
}
// Check if the rotairy has been clicked on
esp_err_t rotairy_driver_is_clicked(rotairy_encoder_info_t * rotairy_encoder_info, bool* result) {
	esp_err_t error = ESP_OK;
	
	uint8_t status = 0;
	error = s_smbus_read_byte(rotairy_encoder_info->smbus_info, rotairy_encoder_info->general_i2c_xMutex, QWIIC_TWIST_STATUS, &status);
	
	if(error == ESP_FAIL){
		return error;
	}
	
	*result = status & (1 << QWIIC_TWIST_STATUS_CLICKED);
	
	error = s_smbus_write_byte(rotairy_encoder_info->smbus_info, rotairy_encoder_info->general_i2c_xMutex, QWIIC_TWIST_STATUS, (status & ~(1 << QWIIC_TWIST_STATUS_CLICKED)));  //We've read this status bit, now clear it
	
	return error;
}

//Returns the number of milliseconds since the last encoder movement
// REMOVED BECAUSE OF MEMORY SHORTAGE
// esp_err_t rotairy_driver_timeSinceLastMovement(rotairy_encoder_info_t * rotairy_encoder_info, uint16_t* result, bool clearValue) {
// 	esp_err_t error = ESP_OK;
	
//     error = s_smbus_read_word(rotairy_encoder_info->smbus_info, rotairy_encoder_info->general_i2c_xMutex, QWIIC_TWIST_TIME_SINCE_LAST_MOVEMENT_LSB, result);
	
// 	//Clear the current value if requested
// 	if (clearValue == true)
// 		error = s_smbus_write_word(rotairy_encoder_info->smbus_info, rotairy_encoder_info->general_i2c_xMutex, QWIIC_TWIST_TIME_SINCE_LAST_MOVEMENT_LSB, (uint16_t)0x00);
  
// 	return error;
// }	

//Returns the number of milliseconds since the last button event (press and release)
// REMOVED BECAUSE OF MEMORY SHORTAGE
// esp_err_t rotairy_driver_timeSinceLastPress(rotairy_encoder_info_t * rotairy_encoder_info, uint16_t* result, bool clearValue) {
// 	esp_err_t error = ESP_OK;
	
// 	error = s_smbus_read_word(rotairy_encoder_info->smbus_info, rotairy_encoder_info->general_i2c_xMutex, QWIIC_TWIST_TIME_SINCE_LAST_BUTTON_LSB, result);
	
// 	//Clear the current value if requested
// 	if (clearValue == true)
// 		error = s_smbus_write_word(rotairy_encoder_info->smbus_info, rotairy_encoder_info->general_i2c_xMutex, QWIIC_TWIST_TIME_SINCE_LAST_BUTTON_LSB, (uint16_t)0x00);
	
// 	return error;
// }
// Get our encoder status. Used for polling.
esp_err_t rotairy_driver_get_status(rotairy_encoder_info_t * rotairy_encoder_info, uint8_t* result) {
	esp_err_t error = ESP_OK;
	error = s_smbus_read_byte(rotairy_encoder_info->smbus_info, rotairy_encoder_info->general_i2c_xMutex, QWIIC_TWIST_STATUS, result);
	
	if(error == ESP_FAIL){
		return error;
	}
	
	error = s_smbus_write_byte(rotairy_encoder_info->smbus_info, rotairy_encoder_info->general_i2c_xMutex, QWIIC_TWIST_STATUS, 0x00);
	
	return error;
}
// Rotairy encoder polling task. 
// Task will request a status register with bit flags to see if/what to ask the rotairy.
void rotairy_driver_task(void* pvParameters)
{
	rotairy_encoder_info_t * rotairy_encoder_info = (rotairy_encoder_info_t *)pvParameters;
    esp_err_t error = ESP_OK;
	uint8_t result = 0;
	uint16_t movement = 0;
	
    while (task_running) {
	
		error = rotairy_driver_get_status(rotairy_encoder_info, &result);
		if (error != ESP_OK) {
			ESP_LOGI(TAG, "Error in task: %d", error);
		}
		
		//Click event
		if ((result & (1<<QWIIC_TWIST_STATUS_CLICKED)) > 0) {
			if (onButtonClicked != NULL) {
				onButtonClicked();
			}
		}
		
		//Pressed event
		if ((result & (1<<QWIIC_TWIST_STATUS_PRESSED)) > 0) {
			if (onButtonPressed != NULL) {
				onButtonPressed();
			}
		}
		
		//Moved event
		if ((result & (1<<QWIIC_TWIST_STATUS_MOVED)) > 0) {
			if (onRotairyTurned != NULL) {
				// Get the amount of movement
				
				rotairy_driver_get_diff(rotairy_encoder_info, &movement, true);
				onRotairyTurned(movement);
			}
		 }
		vTaskDelay(pdMS_TO_TICKS(ROTAIRY_DRIVER_POLLING_TASK_TIME));
    }
	
	//ESP_LOGI(TAG, "task stopped");

    vTaskDelete(NULL);
}

esp_err_t rotairy_driver_start_task(rotairy_encoder_info_t * rotairy_encoder_info) {
	task_running = true;
	
	//ESP_LOGI(TAG, "Starting task");

	xTaskCreate(&rotairy_driver_task, "rotairy_driver_task", 1024 * 4,(void*)rotairy_encoder_info, ROTAIRY_DRIVER_POLLING_TASK_PRO, NULL);	
	return ESP_OK;
}

esp_err_t rotairy_driver_stop_task() {
	task_running = false;

	//ESP_LOGI(TAG, "Stopping task");

	return ESP_OK;
}



