#ifndef ROTAIRY_DRIVER_H
#define ROTAIRY_DRIVER_H

#include "esp_err.h"
#include "smbus.h"
#include <stdint.h>
#include <stdbool.h>
#include <math.h>

/**
* - This driver will support only one rotairy encoder.
* - Standard address: 0x3F.
* - Usage of s_smbus will make it threadsafe if all other
*   i2c devices use s_smbus.
*/

#define ROTAIRY_I2C_ADDRESS 0x3F

#define ROTAIRY_DRIVER_POLLING_TASK_TIME 200
#define ROTAIRY_DRIVER_POLLING_TASK_PRO 1

#define QWIIC_TWIST_STATUS_CLICKED 2
#define QWIIC_TWIST_STATUS_PRESSED 1
#define QWIIC_TWIST_STATUS_MOVED 0

/**
 * @brief
 * struct which holds all information for the smbus
 * and a application level general i2c mutex, to 
 * use mutual exculsion for i2c traffic.
 */
typedef struct {
	bool task_running;
	smbus_info_t * smbus_info;
	SemaphoreHandle_t * general_i2c_xMutex;
} rotairy_encoder_info_t;

typedef uint8_t qwiic_twist_reg_t;

/**
 * @brief
 * all the approachable register addresses. These
 * addresses are used to read the rotairy encoder.
 */
enum qwiic_twist_reg {
	QWIIC_TWIST_STATUS = 0x01,  //2 - button clicked, 1 - button pressed, 0 - encoder moved
	QWIIC_TWIST_VERSION = 0x02,
	QWIIC_TWIST_INT_ENABLE = 0x04,
	QWIIC_TWIST_ENCODER_COUNT_LSB = 0x05,
    QWIIC_TWIST_ENCODER_COUNT_MSB = 0x06,
    QWIIC_TWIST_ENCODER_DIFFERENCE_LSB = 0x07,
	QWIIC_TWIST_ENCODER_DIFFERENCE_MSB = 0x08,
	QWIIC_TWIST_TIME_SINCE_LAST_MOVEMENT_LSB = 0x09,
	QWIIC_TWIST_TIME_SINCE_LAST_MOVEMENT_MSB = 0x0A,
	QWIIC_TWIST_TIME_SINCE_LAST_BUTTON_LSB = 0x0B,
	QWIIC_TWIST_TIME_SINCE_LAST_BUTTON_MSB = 0x0C,
	QWIIC_TWIST_LED_BRIGHTNESS_RED = 0x0D,
	QWIIC_TWIST_LED_BRIGHTNESS_GREEN = 0x0E,
	QWIIC_TWIST_LED_BRIGHTNESS_BLUE = 0x0F,
	QWIIC_TWIST_TURN_INTERRUPT_TIMEOUT_LSB = 0x16
};

/**
 * @brief
 * This method will set 3 callbacks.
 * -onRotairyTurned
 * 		Will be called when rotairy moved status flag is on.
 * -onButtonCLicked
 * 		Will be called when a full click has been done. in and out.
 *  -onButtonPressed
 * 		Will be called when the rotairy is pressed. Repeated when holding.
 */
esp_err_t rotairy_driver_init(void (*onRotairyTurned_p)(int16_t), 
								void (*onButtonClicked_p)(void), 
								void (*onButtonPressed_p)(void));

/**
 * @brief
 * Allocation for a info struct
 */
rotairy_encoder_info_t * rotairy_info_malloc(void);
/**
 * @brief
 * Sets the RGB brightnes to the rotairy encoder.
 * 
 * @param[in] r	brightness value red 0 to 255.
 * @param[in] g	brightness value green 0 to 255.
 * @param[in] b	brightness value blue 0 to 255.
 * @param[in] rotairy_encoder_info	info for smbus (port and address) and mutex.
 */
esp_err_t rotairy_driver_set_color(rotairy_encoder_info_t * rotairy_encoder_info, uint8_t r, uint8_t g, uint8_t b);
/**
 * @brief
 * Gets the RGB brightnes to the rotairy encoder.
 * 
 * @param[out] r	brightness value red 0 to 255.
 * @param[out] g	brightness value green 0 to 255.
 * @param[out] b	brightness value blue 0 to 255.
 * @param[in] rotairy_encoder_info	info for smbus (port and address) and mutex.
 */
esp_err_t rotairy_driver_get_color(rotairy_encoder_info_t * rotairy_encoder_info,uint8_t *r, uint8_t *g, uint8_t *b);
/**
 * @brief
 * Get rotairy encoder count.
 * @param[out] count a unsigned 16-bit integer, which represents count value 32767 and -32768.
 *  				 value can be casted to signed int to use negative values.
 * @param[in] rotairy_encoder_info	info for smbus (port and address) and mutex.
 */
esp_err_t rotairy_driver_get_count(rotairy_encoder_info_t * rotairy_encoder_info,uint16_t *count);
/**
 * @brief
 * Set rotairy encoder count.
 * @param[out] count a unsigned 16-bit integer, which represents count value 32767 and -32768.
 *  				 value needs to be casted to unsigned int to use function.
 * @param[in] rotairy_encoder_info	info for smbus (port and address) and mutex.
 */
esp_err_t rotairy_driver_set_count(rotairy_encoder_info_t * rotairy_encoder_info,uint16_t count);
/**
 * @brief
 * Get rotairy encoder count difference.
 * @param[out] count a value which represents the delta count when called and previous value.
 * @param[in] clearValue if yes: clears count so you can rely on dif between calls. if no: won't clear.
 * @param[in] rotairy_encoder_info	info for smbus (port and address) and mutex.
 */
esp_err_t rotairy_driver_get_diff(rotairy_encoder_info_t * rotairy_encoder_info,uint16_t *count, bool clearValue);
/**
 * @brief
 * Reads a true or false value from rotairy encoder.
 * @param[out] result returns true if count is unread. returns false if movements have been read
 * 					  and no new count.
 * @param[in] rotairy_encoder_info	info for smbus (port and address) and mutex.
 */
esp_err_t rotairy_driver_is_moved(rotairy_encoder_info_t * rotairy_encoder_info, bool* result);
/**
 * @brief
 * Reads a true or false value from rotairy encoder.
 * @param[out] result returns true if clickvalue is unread. returns false if clicks have been read
 * 					  and no new clicks available.
 * @param[in] rotairy_encoder_info	info for smbus (port and address) and mutex.
 */
esp_err_t rotairy_driver_is_clicked(rotairy_encoder_info_t * rotairy_encoder_info, bool* result);
/**
 * @brief
 * Returns time since last movement.
 * @param[out] result Value between 0 and 65535 indicating the number of
 *					  milliseconds since last movement.
 * @param[in] clearValue if true: clears value after call. if false, won't clear value.
 * @param[in] rotairy_encoder_info	info for smbus (port and address) and mutex.
 */
esp_err_t rotairy_driver_timeSinceLastMovement(rotairy_encoder_info_t * rotairy_encoder_info, uint16_t* result, bool clearValue);
/**
 * @brief
 * Returns time since last press.
 * @param[out] result Value between 0 and 65535 indicating the number of
 *					  milliseconds since last press.
 * @param[in] clearValue if true: clears value after call. if false, won't clear value.
 * @param[in] rotairy_encoder_info	info for smbus (port and address) and mutex.
 */
esp_err_t rotairy_driver_timeSinceLastPress(rotairy_encoder_info_t * rotairy_encoder_info, uint16_t* result, bool clearValue);
/**
 * @brief
 * returns result with bit flags:
 * Bit(0) = encoder interrupt.
 * Bit(1) = button interrupt.
 * Bit(2) = button clicked.
 * Bit(3) = button pressed.
 * @param[out] result a unsigned byte with bit flags. see brief.
 * @param[in] rotairy_encoder_info	info for smbus (port and address) and mutex.
 */
esp_err_t rotairy_driver_get_status(rotairy_encoder_info_t * rotairy_encoder_info, uint8_t *result);
/**
 * @brief
 * This method will launch a polling task. this task will poll each amount (defintion ROTAIRY_DRIVER_POLLING_TASK_TIME)
 * of milisecons. This task will request the status with bit flags from rotairy encoder.
 * @param[in] rotairy_encoder_info	info for smbus (port and address) and mutex.
 */
esp_err_t rotairy_driver_start_task(rotairy_encoder_info_t * rotairy_encoder_info);
/**
 * @brief
 * This method will stop the runnning task, if running.
 */
esp_err_t rotairy_driver_stop_task();

#endif