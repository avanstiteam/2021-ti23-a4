/*lcd menu header file*/

#include "i2c-lcd1602.h"

#ifndef LCD_MENU_H
#define LCD_MENU_H

/**
 * @brief inits the lcd and draws the menu
 * 
 * @param[in] xMutex pointer to the mutex to use with i2c operation.
 */
void init_LCD_menu(SemaphoreHandle_t *xMutex);

/**
 * @brief draws the looper menu
 */
void draw_looper(void);

/**
 * @brief draws the clock menu
 */
void draw_clock(void);

/**
 * @brief draws the fm menu
 */
void draw_fm(void);

/**
 * @brief starts the recording of a segment
 */
void record_segment(void);

/**
 * @brief plays a given segment
 */
void play_segment(void);

/**
 * @brief displays the clock settings
 */
void draw_clock_settings(void);

/**
 * @brief draws the main menu
 */
void draw_main_menu(void);

/**
 * @brief plays the radio
 */
void play_radio(void);

/**
 * @brief pauses the radio
 */
void stop_radio(void);

/**
 * @brief draws the time and date on the right of the display
 * 
 */
void draw_time_date();

/**
 * @brief initializes the LCD with i2c.
 * 
 * @return a new i2c_lcd1602_info_t struct holding the information of the LCD display
 */
i2c_lcd1602_info_t *init_LCD();

/**
 * @brief draws the menu with the current selected position and menu item
 * 
 */
void draw_menu();

/**
 * @brief sets the current menu to the given menu.
 * 
 * @param[in] menu the menu to set
 */
void set_menu(uint8_t menu);

/**
 * @brief clicks the selected option
 */
void click();

/**
 * @brief clicks the selected option and then draws the menu
 */
void click_and_draw();

/**
 * @brief sets the currently drawn date to the given date. NOTE: only the value of the date will be set, the display will still need to be updated.
 * 
 * @param[in] date the string containing the date
 */
void set_date(char *date);

/**
 * @brief sets the currently drawn time to the given time. NOTE: only the value of the time will be set, the display will still need to be updated.
 * 
 * @param[in] time the string containing the date
 */
void set_time(char *time);

/**
 * @brief sets the callback that gets called when the "play" item in the looper menu is called.
 * 
 * @param callback the callback that should be called. It's a function pointer to a void function.
 */
void set_menu_play_callback(void (*callback)(void));

/**
 * @brief sets the callback that gets called when the "stop" item in the looper menu is called.
 * 
 * @param callback the callback that should be called. It's a function pointer to a void function.
 */
void set_menu_stop_callback(void (*callback)(void));

/**
 * @brief sets the callback that gets called when the rotary encoder is turned right(up) in the volume settings menu.
 * 
 * @param callback the callback that should be called. It's a function pointer to a void function.
 */
void set_menu_volume_up_callback(void (*callback)(void));

/**
 * @brief sets the callback that gets called when the rotary encoder is turned left(down) in the volume settings menu.
 * 
 * @param callback the callback that should be called. It's a function pointer to a void function.
 */
void set_menu_volume_down_callback(void (*callback)(void));

/**
 * @brief sets the callback that gets called when the rotary encoder is turned right(up) in the station selector menu.
 * 
 * @param callback the callback that should be called. It's a function pointer to a void function.
 */
void set_menu_station_up_callback(void (*callback)(void));

/**
 * @brief sets the callback that gets called when the rotary encoder is turned left(down) in the station selector menu.
 * 
 * @param callback the callback that should be called. It's a function pointer to a void function.
 */
void set_menu_station_down_callback(void (*callback)(void));

/** @brief sets the callback that gets called when the "tell time" option in the clock menu is selected.
 * 
 * @param callback the callback that should be called. It's a function pointer to a void function.
 */
void set_menu_tell_time_callback(void (*callback)(void));

/** 
 * @brief sets the callback that gets called when the "tell time" option in the clock menu is selected.
 * 
 * @param callback the callback that should be called. It's a function pointer to a void function.
 */
void set_menu_metronome_on_change_BPM(void (*callback)(uint16_t, uint8_t *));

/** 
 * @brief sets the callback that gets called when the "tell time" option in the clock menu is selected.
 * 
 * @param callback the callback that should be called. It's a function pointer to a void function.
 */
void set_menu_metronome_start(void (*callback)(void));

/** 
 * @brief sets the callback that gets called when the "tell time" option in the clock menu is selected.
 * 
 * @param callback the callback that should be called. It's a function pointer to a void function.
 */
void set_menu_metronome_stop(void (*callback)(void));

/**
 * @brief sets the callback that gets called when the radio stop option gets called.
 * 
 * @param callback the callback that should be called. It's a function pointer to a void function.
 */
void set_menu_radio_stop_callback(void (*callback)(void));

/**
 * @brief sets the callback that gets called when the radio play option gets called.
 * 
 * @param callback the callback that should be called. It's a function pointer to a void function.
 */
void set_menu_radio_play_callback(void (*callback)(void));

/**
 * @brief sets the current menu to the given menu, and then draws it.
 */
void set_and_draw_menu(uint8_t menu);

/**
 * @brief scrolls the menu the specified amount of places.
 * 
 * @param[in] amount  the amount of spaces that need to be scrolled
 */
void scroll_select(int16_t amount);

/**
 * @brief scrolls the menu the specified amount of places and then draws the screen.
 * 
 * @param[in] amount  the amount of spaces that need to be scrolled
 */
void scroll_and_draw(int16_t amount);

/**
 * @brief gets the currently selected menu
 * 
 * @param[in,out] r red value character pointer
 * @param[in,out] g green value character pointer
 * @param[in,out] b blue value character pointer
 */
void pass_current_menu_color(uint8_t *r, uint8_t *g, uint8_t *b);

/**
 * @brief turns the LCD backlight on or off depending on the given value
 * 
 * @param[in] bool True to enable, false to disable.
 * @return ESP_OK if successful, otherwise an error constant.
 */
esp_err_t set_LCD_backlight(bool enable);

#endif