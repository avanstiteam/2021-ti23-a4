/*LCD menu file*/

#include <stdio.h>
#include <string.h>
#include <math.h>

#include "lcd_menu.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "driver/gpio.h"
#include "driver/i2c.h"
#include "esp_log.h"
#include "sdkconfig.h"
#include "esp32/rom/uart.h"

// #include "smbus.h"
#include "i2c-lcd1602.h"

/*lcd specific defines*/

#define LCD_NUM_ROWS 4
#define LCD_NUM_ROWS_AVAILABLE 3
#define LCD_NUM_COLUMNS 40
#define LCD_NUM_VISIBLE_COLUMNS 20

#define I2C_MASTER_NUM I2C_NUM_0
#define LCD1602_I2C_ADDRESS 0x27

/*menu related defines*/
#define TIME_START_COL 15
#define TIME_START_ROW 1
#define DATE_START_COL 14
#define DATE_START_ROW 2

#define MENU_MAIN_ID 0
#define MENU_LOOPER_ID 1
#define MENU_CLOCK_ID 2
#define MENU_FM_ID 3
#define MENU_VOLUME_ID 4
#define MENU_METRONOME_ID 5
#define MENU_VOLUME_SETTINGS_ID 6
#define MENU_LOOPER_RECORD_OPTIONS_ID 7
#define MENU_FM_STATION_ID 8
#define MENU_METRONOME_SETTINGS_ID 9

#define MIN_MENU_INDEX 0
#define MAX_MENU_INDEX 9

#define MAX_OPTIONS 5
#define SELECT_ROW 2

#define TIME_PLACEHOLDER "17:04"
#define DATE_PLACEHOLDER "mar 08"

// const static char *TAG = "LCD MENU";
const static char *fpTAG = "FUNCTION POINTER";
static i2c_lcd1602_info_t *lcd_info;
static signed char select_pos = 0; /*selected item in the menu*/
static uint8_t selected_menu = MENU_MAIN_ID;

// static int changingVolmue = 0;

static void (*menu_on_play_callback)(void); // callback to when the play button gets pressed
static void (*menu_on_stop_callback)(void);
static void (*menu_volume_up_callback)(void);
static void (*menu_volume_down_callback)(void);
static void (*menu_station_up_callback)(void);
static void (*menu_station_down_callback)(void);
static void (*menu_on_tell_time_callback)(void);
static void (*menu_on_change_BPM)(uint16_t, uint8_t *);
static void (*menu_metronome_start)(void);
static void (*menu_metronome_stop)(void);
static void (*menu_play_radio_callback)(void);
static void (*menu_stop_radio_callback)(void);

static char *current_date = "mar 08";
static char *current_time = "17:04";

static char menu_bpm_title[10] = "1 BPM";

void volume_down(void);
void volume_up(void);
void station_up(int);
void station_down(int);
static bool time_init = false; // wether or not the wifi time has initted
static bool date_init = false; // wether or not the wifi date has initted

#define DATE_TIME_INIT (date_init && time_init)

/**
 * @brief struct to hold a menu item
 */
typedef struct
{
    uint8_t ID;      /*id*/
    uint8_t options; /*amount of selectable options*/
    uint8_t r;
    uint8_t g;
    uint8_t b;
    char title[LCD_NUM_VISIBLE_COLUMNS]; /*title*/
    char *lines[MAX_OPTIONS];            /*text to display*/
    void (*fpOnItem[MAX_OPTIONS])(void); /*function pointers to get called when an item is selected on the specific row*/

} MENU_ITEM;

void set_menu(uint8_t menu)
{
    select_pos = 0;
    selected_menu = menu;
}

void draw_looper(void)
{
    //ESP_LOGI(fpTAG, "drawing looper");
    set_menu(MENU_LOOPER_ID);
}

void draw_clock(void)
{
    //ESP_LOGI(fpTAG, "drawing clock");
    set_menu(MENU_CLOCK_ID);
}

void draw_fm(void)
{
    // ESP_LOGI(fpTAG, "drawing fm");
    set_menu(MENU_FM_ID);
}

void draw_volume(void)
{
    // ESP_LOGI(fpTAG, "drawing volume");
    set_menu(MENU_VOLUME_ID);
}

void draw_main_menu(void)
{
    // ESP_LOGI(fpTAG, "drawing main");
    set_menu(MENU_MAIN_ID);
}

void draw_station_selector(void)
{
    // ESP_LOGI(fpTAG, "drawing station selector");
    set_menu(MENU_FM_STATION_ID);
}

void draw_looper_record_options(void)
{
    // ESP_LOGI(fpTAG, "drawing looper record options");
    set_menu(MENU_LOOPER_RECORD_OPTIONS_ID);
}

void record_segment(void)
{
    // ESP_LOGI(fpTAG, "recording segment");
    draw_looper_record_options();
}

void play_segment(void)
{
    // ESP_LOGI(fpTAG, "playing segment");
    // ESP_LOGI(fpTAG, "calling on play callback");
    if (menu_on_play_callback != NULL)
        (*menu_on_play_callback)();
    else
        ESP_LOGE(fpTAG, "ERROR: menu_on_play_callback was null!");
}

void change_volume(int16_t amount)
{
    // ESP_LOGI(fpTAG, "changing volume");

    if (amount > 0)
    {
        volume_up();
    }
    if (amount < 0)
    {
        volume_down();
    }
}

void change_station(int16_t amount)
{
    // ESP_LOGI(fpTAG, "changing station");

    if (amount > 0)
    {
        station_up(amount);
    }
    if (amount < 0)
    {
        station_down(amount);
    }
}

void tell_time(void)
{
    // ESP_LOGI(fpTAG, "telling the time");
    if (menu_on_tell_time_callback != NULL)
        (*menu_on_tell_time_callback)();
    else
        ESP_LOGE(fpTAG, "ERROR: menu_on_play_callback was null!");
}
void stop_segment(void)
{
    // ESP_LOGI(fpTAG, "calling on stop callback");
    if (menu_on_stop_callback != NULL)
        (*menu_on_stop_callback)();
    else
        ESP_LOGW(fpTAG, "ERROR: menu_on_stop_callback was null!");
}

void draw_volume_settings(void)
{
    // ESP_LOGI(fpTAG, "drawing volume bar settings");
    set_menu(MENU_VOLUME_SETTINGS_ID);
}

void draw_metronome(void)
{
    ESP_LOGI(fpTAG, "drawing metronome settings");

    set_menu(MENU_METRONOME_ID);
}

void draw_metronome_settings()
{
    ESP_LOGI(fpTAG, "drawing metronome BPM");
    menu_metronome_stop();
    set_menu(MENU_METRONOME_SETTINGS_ID);
}

void set_metronome(void)
{
    ESP_LOGI(fpTAG, "metronome set");
}

void start_metronome(void)
{
    ESP_LOGI(fpTAG, "metronome start");
    menu_metronome_start();
}

void stop_metronome(void)
{
    ESP_LOGI(fpTAG, "metronome stop");
    menu_metronome_stop();
}

void play_radio(void)
{
    if (menu_play_radio_callback != NULL)
        (*menu_play_radio_callback)();
    else
        ESP_LOGE(fpTAG, "ERROR: menu_on_stop_callback was null!");
}

void stop_radio(void)
{
    if (menu_stop_radio_callback != NULL)
        (*menu_stop_radio_callback)();
}

void change_date(void)
{
    // ESP_LOGI(fpTAG, "changing date");
}

void change_time(void)
{
    
}

void downmix(void)
{
    // ESP_LOGI(fpTAG, "downmixing...");
}

/**
 * @brief the array of menu item structs that acts as the entire menu
 */
MENU_ITEM menu[] = {
    {MENU_MAIN_ID, /*menu id*/
     MAX_OPTIONS,
     255,
     0,
     0,
     "MAIN MENU", /*title*/
     {            /*text items*/
      "Looper",
      "Clock",
      "FM",
      "Volume",
      "Metrome"},
     /*function pointers to the functions to be called when the enter button is pressed on a specific line*/
     {
         draw_looper,
         draw_clock,
         draw_fm,
         draw_volume,
         draw_metronome}

    },
    {MENU_LOOPER_ID,
     4,
     0,
     255,
     0,
     "LOOPER",
     {"record",
      "play",
      "stop",
      "<--"},
     {record_segment, play_segment, stop_segment, draw_main_menu}},
    {MENU_CLOCK_ID,
     2,
     0,
     0,
     255,
     "CLOCK",
     {
      "Tell time",
      "<--"},
     {tell_time, draw_main_menu}},
    {MENU_FM_ID,
     4,
     0,
     255,
     255,
     "FM",
     {"Station",
      "Play",
      "Pause",
      "<--"},
     {draw_station_selector, play_radio, stop_radio, draw_main_menu}},
    {MENU_VOLUME_ID,
     2,
     255,
     255,
     0,
     "VOLUME",
     {"-[**********]+",
      "<--"},
     {draw_volume_settings, draw_main_menu}},
    {MENU_METRONOME_ID,
     4,
     255,
     255,
     0,
     "METRONOME",
     {menu_bpm_title,
      "Start",
      "Stop",
      "<--"},
     {draw_metronome_settings, start_metronome, stop_metronome, draw_main_menu}},
    {MENU_VOLUME_SETTINGS_ID,
     1,
     255,
     255,
     0,
     "VOLUME",
     {"-[**********]+"},
     {draw_volume}},
    {MENU_LOOPER_RECORD_OPTIONS_ID,
     2,
     255,
     255,
     255,
     "DOWNMIX?",
     {"Downmix",
      "Cancel"},
     {downmix, draw_looper}},
    {MENU_FM_STATION_ID,
     1,
     0,
     255,
     0,
     "STATION",
     {"< STATIONNAME >"},
     {draw_fm}},
    {MENU_METRONOME_SETTINGS_ID,
     1,
     255,
     255,
     0,
     "METRONOME",
     {menu_bpm_title},
     {draw_metronome}}};

i2c_lcd1602_info_t *init_LCD(SemaphoreHandle_t *xMutex)
{
    // ESP_LOGI(TAG, "initializing lcd...");
    // Set up I2C
    i2c_port_t i2c_num = I2C_MASTER_NUM;
    uint8_t address = LCD1602_I2C_ADDRESS;

    // Set up the SMBus
    smbus_info_t *smbus_info = smbus_malloc();
    ESP_ERROR_CHECK(smbus_init(smbus_info, i2c_num, address));
    ESP_ERROR_CHECK(smbus_set_timeout(smbus_info, 1000 / portTICK_RATE_MS));

    // Set up the LCD1602 device with backlight off
    i2c_lcd1602_info_t *lcd_info = i2c_lcd1602_malloc();
    // ESP_LOGI(TAG, "attempting to init i2c lcd");
    ESP_ERROR_CHECK(i2c_lcd1602_init(lcd_info, smbus_info, xMutex, true,
                                     LCD_NUM_ROWS, LCD_NUM_COLUMNS, LCD_NUM_VISIBLE_COLUMNS));

    ESP_ERROR_CHECK(i2c_lcd1602_reset(lcd_info));
    // ESP_LOGI(TAG, "done initializing lcd");
    return lcd_info;
}

void volume_up(void)
{
    // ESP_LOGI(fpTAG, "calling volume callback");
    if (menu_volume_up_callback != NULL)
        (*menu_volume_up_callback)();
    else
        ESP_LOGW(fpTAG, "ERROR: menu_volume_up_callback was null!");
    if (menu[selected_menu].g - 32 >= 0)
    {
        menu[selected_menu].g -= 32;
    }
}

void volume_down(void)
{
    // ESP_LOGI(fpTAG, "calling volume callback");
    if (menu_volume_down_callback != NULL)

        (*menu_volume_down_callback)();
    else
        ESP_LOGW(fpTAG, "ERROR: menu_volume_down_callback was null!");

    if (menu[selected_menu].g + 32 <= 255)
    {
        menu[selected_menu].g += 32;
    }
}

void station_up(int amount)
{
    // ESP_LOGI(fpTAG, "calling station up callback");
    if (menu_station_up_callback != NULL)
        (*menu_station_up_callback)();
    else
        ESP_LOGW(fpTAG, "ERROR: menu_station_up_callback was null!");
}

void station_down(int amount)
{
    // ESP_LOGI(fpTAG, "calling station down callback");
    if (menu_station_down_callback != NULL)

        (*menu_station_down_callback)();
    else
        ESP_LOGW(fpTAG, "ERROR: menu_station_down_callback was null!");
}

void change_metronome_bmp(uint16_t amount)
{
    // callback.
    uint8_t new_bpm;
    menu_on_change_BPM(amount, &new_bpm);

    // MENU_ITEM metronome_settings = menu[MENU_METRONOME_SETTINGS_ID];
    // MENU_ITEM metronome = menu[MENU_METRONOME_ID];

    int length = snprintf(NULL, 0, "%d BPM", new_bpm);
    char new_str[length + 1];
    snprintf(new_str, length + 1, "%d BPM", new_bpm);

    strcpy(menu_bpm_title, new_str);
    // strcpy(metronome.lines[0], new_str);

    // int length = snprintf(new_str, 0, "%d BPM", new_bpm);
    // char str[length + 1];
    // snprintf(str, length + 1, "%d", number);
}

void draw_time_date()
{
    if (!(selected_menu == MENU_VOLUME_ID || selected_menu == MENU_VOLUME_SETTINGS_ID || selected_menu == MENU_FM_STATION_ID))
    {
        // check if time and date have been initialized
        current_date = (current_date == NULL || sizeof(current_date) > 7) ? DATE_PLACEHOLDER : current_date;
        current_time = (current_time == NULL || sizeof(current_time) > 6) ? TIME_PLACEHOLDER : current_time;

        i2c_lcd1602_move_cursor(lcd_info, TIME_START_COL, TIME_START_ROW);
        i2c_lcd1602_write_string(lcd_info, current_time);

        i2c_lcd1602_move_cursor(lcd_info, DATE_START_COL, DATE_START_ROW);
        i2c_lcd1602_write_string(lcd_info, current_date);
    }
    else
    {
        return;
    }
}

void draw_menu()
{
    // char *tag = "[DRAW MENU]";

    if (DATE_TIME_INIT)
    {

        MENU_ITEM menu_to_draw;

        menu_to_draw = menu[selected_menu];
        // ESP_LOGI(tag, "Drawing menu with id %d", menu_to_draw.ID);

        i2c_lcd1602_clear(lcd_info);

        int elements = strlen(menu_to_draw.title); /*get the length of the title*/
        /*calculate midway point*/
        uint8_t start_col = (LCD_NUM_VISIBLE_COLUMNS / 2) - (elements / 2);

        /*move to start point and draw title*/
        i2c_lcd1602_move_cursor(lcd_info, start_col, 0);
        i2c_lcd1602_write_string(lcd_info, menu_to_draw.title);

        /*********************************************************************/
        /*                      Displaying the current position*/
        /*calculate the lenght of the char array we will be making*/
        int length = snprintf(NULL, 0, "%d/", select_pos + 1);
        char *str = malloc(length + 1);
        snprintf(str, length + 1, "%d/", select_pos + 1);
        i2c_lcd1602_move_cursor(lcd_info, 0, 0);
        i2c_lcd1602_write_string(lcd_info, str);

        int tot_length = snprintf(NULL, 0, "%d", menu_to_draw.options);
        char *tot_str = malloc(tot_length + 1);
        snprintf(tot_str, length + 1, "%d", menu_to_draw.options);
        i2c_lcd1602_write_string(lcd_info, tot_str);

        free(str);
        free(tot_str);

        /*********************************************************************/
        i2c_lcd1602_move_cursor(lcd_info, 0, SELECT_ROW - 1);

        if (select_pos != 0)
        {
            i2c_lcd1602_write_string(lcd_info, menu_to_draw.lines[select_pos - 1]);
        }
        else
            i2c_lcd1602_write_string(lcd_info, " --");

        i2c_lcd1602_move_cursor(lcd_info, 0, SELECT_ROW);
        i2c_lcd1602_write_char(lcd_info, '>');
        i2c_lcd1602_move_cursor(lcd_info, 2, SELECT_ROW);
        i2c_lcd1602_write_string(lcd_info, menu_to_draw.lines[select_pos]);

        i2c_lcd1602_move_cursor(lcd_info, 0, SELECT_ROW + 1);
        if (select_pos != (menu_to_draw.options - 1))
            i2c_lcd1602_write_string(lcd_info, menu_to_draw.lines[select_pos + 1]);
        else
            i2c_lcd1602_write_string(lcd_info, " --");

        draw_time_date();
    }
    else
    {
        char *text = "Initializing...";
        int length = strlen(text);
        int start_col = (LCD_NUM_VISIBLE_COLUMNS / 2) - (length / 2);

        i2c_lcd1602_move_cursor(lcd_info, start_col, 1);
        i2c_lcd1602_write_string(lcd_info, text);
    }
}

void scroll_select(int16_t amount)
{
    // char *scroll_tag = "[SCROLL OPTIONS]";
    // ESP_LOGI(scroll_tag, "scrolling %d", amount);

    select_pos += amount;

    if (select_pos < 0)
        select_pos = 0;
    else
    {
        MENU_ITEM s_menu = menu[selected_menu];

        if (select_pos >= s_menu.options)
            select_pos = s_menu.options - 1;
    }

    // ESP_LOGI(scroll_tag, "selected position is now %d", select_pos);
}

void scroll_and_draw(int16_t amount)
{
    if (selected_menu == MENU_VOLUME_SETTINGS_ID)
    {
        change_volume(amount);
    }
    else if (selected_menu == MENU_METRONOME_SETTINGS_ID)
    {
        change_metronome_bmp(amount);
    }
    else if (selected_menu == MENU_FM_STATION_ID)
    {
        change_station(amount);
        draw_menu();
    }
    else
    {
        scroll_select(amount);
    }

    draw_menu();
}

/**
 * @brief increments the menu by 1, used for debug purposes.
 */
void inc_menu()
{
    selected_menu++;

    if (selected_menu > MAX_MENU_INDEX)
        selected_menu = MIN_MENU_INDEX;
    select_pos = 0;
    // ESP_LOGI(TAG, "selected menu is now %d", selected_menu);
}

void set_date(char *date)
{
    // ESP_LOGI(TAG, "setting date");
    strcpy(current_date, date);
    date_init = true;
}

void set_time(char *time)
{
    // ESP_LOGI(TAG, "setting time");
    strcpy(current_time, time);
    time_init = true;
}

void set_menu_play_callback(void (*callback)(void))
{
    menu_on_play_callback = callback;
}

void set_menu_stop_callback(void (*callback)(void))
{
    menu_on_stop_callback = callback;
}

void set_menu_radio_play_callback(void (*callback)(void))
{
    menu_play_radio_callback = callback;
}

void set_menu_radio_stop_callback(void (*callback)(void))
{
    menu_stop_radio_callback = callback;
}
void set_menu_tell_time_callback(void (*callback)(void))
{
    menu_on_tell_time_callback = callback;
}

void set_menu_volume_up_callback(void (*callback)(void))
{
    menu_volume_up_callback = callback;
}

void set_menu_volume_down_callback(void (*callback)(void))
{
    menu_volume_down_callback = callback;
}

void set_menu_station_up_callback(void (*callback)(void))
{
    menu_station_up_callback = callback;
}

void set_menu_station_down_callback(void (*callback)(void))
{
    menu_station_down_callback = callback;
}
void set_menu_metronome_on_change_BPM(void (*callback)(uint16_t, uint8_t *))
{
    menu_on_change_BPM = callback;
}

void set_menu_metronome_start(void (*callback)(void))
{
    menu_metronome_start = callback;
}

void set_menu_metronome_stop(void (*callback)(void))
{
    menu_metronome_stop = callback;
}

void click()
{

    // ESP_LOGI(TAG, "clicking item %d", select_pos);

    // call the function pointer at the position we have selected on the current menu,
    // only if it is not null
    if (NULL != menu[selected_menu].fpOnItem[select_pos])
        (*menu[selected_menu].fpOnItem[select_pos])();
}

esp_err_t set_LCD_backlight(bool enable)
{
    return i2c_lcd1602_set_backlight(lcd_info, enable);
}

void set_and_draw_menu(uint8_t menu)
{
    set_menu(menu);
    draw_menu();
}

void click_and_draw()
{

    click();
    draw_menu();
}

void display_all_options()
{
    for (size_t i = 0; i < MAX_MENU_INDEX; i++)
    {
        draw_menu();
        for (size_t j = 0; j < menu[selected_menu].options; j++)
        {
            draw_menu();
            scroll_select(1);
            vTaskDelay(200);
        }

        inc_menu();
        vTaskDelay(200);
    }
}

void pass_current_menu_color(uint8_t *r, uint8_t *g, uint8_t *b)
{

    *r = menu[selected_menu].r;
    *g = menu[selected_menu].g;
    *b = menu[selected_menu].b;
}

/**
 * @brief function that holds the logic for the FreeRTOS task that handles all the drawing of the display in debug mode.
 */
void xLcdTask(void *pvParameters)
{
    // ESP_LOGI(TAG, "starting lcd task");

    draw_menu();

    while (1)
    {
        scroll_select(13);
        draw_menu();
        vTaskDelay(600);
        scroll_select(-3);
        draw_menu();
        vTaskDelay(600);
    }
}

void init_LCD_menu(SemaphoreHandle_t *xMutex)
{
    // ESP_LOGI(TAG, "Hello there!");
    lcd_info = init_LCD(xMutex);
    current_date = (char *)malloc((sizeof(char)) * 7);
    current_time = (char *)malloc((sizeof(char)) * 6);
    draw_menu();

    // if we want to automatically scroll through all options on each menu:
    // xTaskCreate(&xLcdTask, "LCD task", 2048, NULL, 1, NULL);
}
