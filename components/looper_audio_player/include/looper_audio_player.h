#pragma once
#ifndef LOOPER_AUDIO_PLAYER_H
#define LOOPER_AUDIO_PLAYER_H
#include "esp_err.h"
#include "esp_peripherals.h"
#include "freertos/queue.h"
#include "freertos/semphr.h"

#define LOOPER_AUDIO_IT_IS_NOW 0
#define LOOPER_AUDIO_O_CLOCK 1
#define LOOPER_AUDIO_HOUR 2
#define LOOPER_AUDIO_HOURS 3
#define LOOPER_AUDIO_MINUTE 4
#define LOOPER_AUDIO_MINUTES 5
#define LOOPER_AUDIO_AND 6
#define LOOPER_AUDIO_ZERO 7
#define LOOPER_AUDIO_AUD 68

/**
 * @brief Initiates looper audio player and sets pipeline up
 * 
 * @param set the esp_periph_set_handle_t
 * @param queue the queue with witch it receives the index with of the new file to play
 */
esp_err_t looper_audio_player_init(esp_periph_set_handle_t set, QueueHandle_t queue, SemaphoreHandle_t audio_output_mutex);

/**
 * @brief starts playing audio out of queue, or resumes playing audio if looper_audio_player_pause was called. \n Does noting if audio is already playing
 * 
 */

void looper_audio_player_play();
/**
 * @brief pauses audio pipeline. can be resumed by calling looper_audio_player_play.
 * 
 */
void looper_audio_player_pause();

/**
 * @brief stops playing audio and clears queue.
 * 
 */
void looper_audio_player_stop();
#endif