#include <string.h>

#include "looper_audio_player.h"
#include "freertos/queue.h"
#include "esp_log.h"
#include "audio_pipeline.h"
#include "i2s_stream.h"
#include "wav_decoder.h"
#include "fatfs_stream.h"

#define TAG "looper audio player"

#define LOOPER_AUDIO_ROOT "/sdcard/"
#define LOOPER_AUDIO_FILE_EXTENSION ".wav"
#define LOOPER_AUDIO_LANGUAGE "de/"

static char audio_files[][34] = {
    "now",
    "oclk",
    "hour",
    "hours",
    "min",
    "mins",
    "and",
    "0",
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "10",
    "11",
    "12",
    "13",
    "14",
    "15",
    "16",
    "17",
    "18",
    "19",
    "20",
    "21",
    "22",
    "23",
    "24",
    "25",
    "26",
    "27",
    "28",
    "29",
    "30",
    "31",
    "32",
    "33",
    "34",
    "35",
    "36",
    "37",
    "38",
    "39",
    "40",
    "41",
    "42",
    "43",
    "44",
    "45",
    "46",
    "47",
    "48",
    "49",
    "50",
    "51",
    "52",
    "53",
    "54",
    "55",
    "56",
    "57",
    "58",
    "59",
    "60",
    "aud"};

static audio_pipeline_handle_t pipeline;
static audio_element_handle_t i2s_stream_writer, wav_decoder, fatfs_stream_reader;
static audio_event_iface_handle_t evt;
static QueueHandle_t input_queue;
static SemaphoreHandle_t pipeline_mutex;
static SemaphoreHandle_t output_mutex;

/**
 * @brief Get the file path on the sdcard [ROOT][LANGUAGE][audio_files[file]][FILE_EXTENSION]
 * 
 * @param buffer the buffer in witch the path is stored
 * @param len the length of the buffer (if te compiler veraion is above 11)
 * @param file the element of te filename in audiofiles
 */
static void get_path(char buffer[], int len, int file);

/**
 * @brief the looper_audio_player_queue_listener task. sets correct sample rate for i2s stream and starts new audio file from the input queue when file is finished.
 * 
 * @param[in] pvParameters not used
 */
static void looper_audio_player_queue_listener(void *pvParameters)
{
    while (1)
    {
        /* Handle event interface messages from pipeline
           to set music info and to advance to the next song
        */
        audio_event_iface_msg_t msg;
        esp_err_t ret = audio_event_iface_listen(evt, &msg, portMAX_DELAY);
        if (ret != ESP_OK)
        {
            ESP_LOGE(TAG, "[ * ] Event interface error : %d", ret);
            continue;
        }
        if (msg.source_type == AUDIO_ELEMENT_TYPE_ELEMENT)
        {
            // Set music info for a new song to be played
            if (msg.source == (void *)wav_decoder && msg.cmd == AEL_MSG_CMD_REPORT_MUSIC_INFO)
            {
                audio_element_info_t music_info = {};
                audio_element_getinfo(wav_decoder, &music_info);
                // ESP_LOGI(TAG, "[ * ] Received music info from wav decoder, sample_rates=%d, bits=%d, ch=%d",
                        //  music_info.sample_rates, music_info.bits, music_info.channels);
                audio_element_setinfo(i2s_stream_writer, &music_info);
                i2s_stream_set_clk(i2s_stream_writer, music_info.sample_rates, music_info.bits, music_info.channels);
                continue;
            }
            // Advance to the next song when previous finishes
            if (msg.source == (void *)i2s_stream_writer && msg.cmd == AEL_MSG_CMD_REPORT_STATUS)
            {
                audio_element_state_t el_state = audio_element_get_state(i2s_stream_writer);
                // ESP_LOGI(TAG, "el_state task %d", el_state);

                if (el_state == AEL_STATE_FINISHED)
                {
                    int element = 0;
                    if (uxQueueMessagesWaiting(input_queue) > 0 &&
                        xQueueReceive(input_queue, &element, portMAX_DELAY))
                    {
                        // ESP_LOGI(TAG, "Finish sample, towards next sample");
                        char url[50];
                        get_path(url, 50, element);
                        ESP_LOGI(TAG, "Finish sample, towards next sample, URL: %s", url);
                        audio_element_set_uri(fatfs_stream_reader, url);
                        audio_pipeline_reset_ringbuffer(pipeline);
                        audio_pipeline_reset_elements(pipeline);
                        audio_pipeline_change_state(pipeline, AEL_STATE_INIT);
                        audio_pipeline_run(pipeline);
                    }
                    else
                    {
                        // No more samples. Pause for now
                        audio_pipeline_terminate(pipeline);
                        xSemaphoreGive(output_mutex);
                    }
                }
                continue;
            }
        }
    }
}

esp_err_t looper_audio_player_init(esp_periph_set_handle_t set, QueueHandle_t queue, SemaphoreHandle_t audio_output_mutex)
{
    if (pipeline != NULL)
    {
        ESP_LOGW(TAG, "looper_audio_playe already initialized");
        return ESP_FAIL;
    }
    // ESP_LOGI(TAG, "Initializing looper audio player");
    input_queue = queue;
    output_mutex = audio_output_mutex;

    audio_pipeline_cfg_t pipeline_cfg = DEFAULT_AUDIO_PIPELINE_CONFIG();
    pipeline = audio_pipeline_init(&pipeline_cfg);
    mem_assert(pipeline);

    i2s_stream_cfg_t i2s_cfg = I2S_STREAM_CFG_DEFAULT();
    i2s_cfg.type = AUDIO_STREAM_WRITER;
    i2s_stream_writer = i2s_stream_init(&i2s_cfg);

    wav_decoder_cfg_t wav_cfg = DEFAULT_WAV_DECODER_CONFIG();
    wav_decoder = wav_decoder_init(&wav_cfg);

    char *url = NULL;
    fatfs_stream_cfg_t fatfs_cfg = FATFS_STREAM_CFG_DEFAULT();
    fatfs_cfg.type = AUDIO_STREAM_READER;
    fatfs_stream_reader = fatfs_stream_init(&fatfs_cfg);
    audio_element_set_uri(fatfs_stream_reader, url);

    audio_pipeline_register(pipeline, fatfs_stream_reader, "file");
    audio_pipeline_register(pipeline, wav_decoder, "wav");
    audio_pipeline_register(pipeline, i2s_stream_writer, "i2s");

    audio_pipeline_link(pipeline, (const char *[]){"file", "wav", "i2s"}, 3);

    audio_event_iface_cfg_t evt_cfg = AUDIO_EVENT_IFACE_DEFAULT_CFG();
    evt = audio_event_iface_init(&evt_cfg);

    audio_pipeline_set_listener(pipeline, evt);

    pipeline_mutex = xSemaphoreCreateMutex();

    //medium prio
    xTaskCreate(&looper_audio_player_queue_listener, "looper audio player queue listner task", 1024 * 4, NULL, 5, NULL);

    return ESP_OK;
}

void looper_audio_player_play()
{
    audio_element_state_t el_state = audio_element_get_state(i2s_stream_writer);
    // ESP_LOGI(TAG, "el_state play %d", el_state);
    if (el_state == AEL_STATE_PAUSED)
    {
        if (xSemaphoreTake(pipeline_mutex, portMAX_DELAY) == pdTRUE)
        {
            if (xSemaphoreTake(output_mutex, pdMS_TO_TICKS(100)) == pdTRUE)
            {
                audio_pipeline_resume(pipeline);
            }
            xSemaphoreGive(pipeline_mutex);
        }
    }
    else if (el_state != AEL_STATE_RUNNING)
    {
        int element;
        if (uxQueueMessagesWaiting(input_queue) > 0 &&
            xQueueReceive(input_queue, &element, portMAX_DELAY))
        {
            if (xSemaphoreTake(pipeline_mutex, portMAX_DELAY) == pdTRUE)
            {
                if (xSemaphoreTake(output_mutex, pdMS_TO_TICKS(100)) == pdTRUE)
                {
                    char url[40];
                    get_path(url, 40, element);
                    ESP_LOGI(TAG, "URL: %s", url);
                    audio_element_set_uri(fatfs_stream_reader, url); // Set first sample
                    audio_pipeline_reset_ringbuffer(pipeline);
                    audio_pipeline_reset_elements(pipeline);
                    audio_pipeline_change_state(pipeline, AEL_STATE_INIT);
                    audio_pipeline_run(pipeline);
                }
                xSemaphoreGive(pipeline_mutex);
            }
        }
    }
}

void looper_audio_player_pause()
{
    audio_element_state_t el_state = audio_element_get_state(i2s_stream_writer);
    // ESP_LOGI(TAG, "el_state %d", el_state);
    if (el_state == AEL_STATE_RUNNING)
    {
        if (xSemaphoreTake(pipeline_mutex, portMAX_DELAY) == pdTRUE)
        {
            audio_pipeline_pause(pipeline);
            xSemaphoreGive(output_mutex);
            xSemaphoreGive(pipeline_mutex);
        }
    }
}

void looper_audio_player_stop()
{
    audio_element_state_t el_state = audio_element_get_state(i2s_stream_writer);
    if (el_state == AEL_STATE_RUNNING)
    {
        if (xSemaphoreTake(pipeline_mutex, portMAX_DELAY) == pdTRUE)
        {
            xQueueReset(input_queue);
            audio_pipeline_terminate(pipeline);
            audio_pipeline_reset_ringbuffer(pipeline);
            audio_pipeline_reset_elements(pipeline);
            xSemaphoreGive(pipeline_mutex);
            xSemaphoreGive(output_mutex);
        }
    }
}

static void get_path(char buffer[], int len, int file)
{
    // assert(strlen(audio_files[file]) + 16 > len);

#if __cplusplus <= 201103L
    strcpy(buffer, LOOPER_AUDIO_ROOT);
    strcat(buffer, LOOPER_AUDIO_LANGUAGE);
    strcat(buffer, audio_files[file]);
    strcat(buffer, LOOPER_AUDIO_FILE_EXTENSION);
#else
    strcpy_s(buffer, len, LOOPER_AUDIO_ROOT);
    strcat_s(buffer, len, LOOPER_AUDIO_LANGUAGE);
    strcat_s(buffer, len, audio_files[file]);
    strcat_s(buffer, len, LOOPER_AUDIO_FILE_EXTENSION);
#endif
}
