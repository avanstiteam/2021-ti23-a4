#ifndef LOOPER_FREQUENCY_ANALYSIS_H
#define LOOPER_FREQUENCY_ANALYSIS_H

#include "audio_pipeline.h"
#include "raw_stream.h"
#include "goertzel.h"

#define AUDIO_SAMPLE_RATE 48000 // Default sample rate in [Hz]

/**
 * @brief initializes the recording and sampling pipeline. Also starts the FreeRTOS task that listens to the 
 *        input from the micropohone and analyses it to see if it matches one of the given frequencies.
 * 
 * @param[in] raw_handle a pointer to the handle to the raw to read the mic data from.
 */
void frequency_analysis_start_listening();

/**
 * @brief sets the callback that will be called when a frequency is measured after the goertzel analysis that matches one of the frequencies we should check for.
 * 
 * @param callback the function pointer to the function to be called.
 */
void frequency_analysis_set_on_goertzel_callback(void (*callback)(void));

/**
 * stops the task listening for frequencies
 */
void frequency_analysis_stop();

/**
 * @brief sets the callback that will be called when the task has stopped listening.
 * 
 * @param callback the callback that should be called
 */
void frequency_analysis_set_on_stop_callback(void (*callback)(void));

/**
 * @brief sets up the goertzel algorithm elements.
 */
void setup_goertzel();

#endif