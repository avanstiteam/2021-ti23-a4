#include <math.h>

#include "esp_log.h"
#include "audio_common.h"
#include "audio_pipeline.h"
#include "filter_resample.h"
#include "raw_stream.h"
#include "i2s_stream.h"
#include "goertzel.h"
#include "freertos/FreeRTOS.h"

#include "looper_frequency_analysis.h"

static const char *TAG = "LOOPER-FREQUENCY-ANALYSIS";
static int init = 0;

#define GROETZEL_MIN_DB 25.0f
#define GOERTZEL_SAMPLE_RATE_HZ 8000 // Sample rate in [Hz]
#define GOERTZEL_FRAME_LENGTH_MS 50  // Block length in [ms]
#define GOERTZEL_BUFFER_LENGTH (GOERTZEL_FRAME_LENGTH_MS * GOERTZEL_SAMPLE_RATE_HZ / 1000)

#define GOERTZEL_N_DETECTION 1

// clapping is around 1040 Hz
static const int GOERTZEL_DETECT_FREQUENCIES[GOERTZEL_N_DETECTION] = {
    1050};

static goertzel_data_t **goertzel_filt; // goertzel filter
static int16_t *raw_buff;               // buffer for reading raw

static audio_pipeline_handle_t pipeline;                           // pipeline to use
static audio_element_handle_t i2s_stream_reader, filter, raw_read; // audio handles to use
static bool loop = true;

static void (*on_goertzel_callback)(void); // callback for when a goertzel result is processed
static void (*on_listening_stopped_callback)(void);

/**
 * @brief callback that gets called when a goertzel analysis is completed.
 * 
 * @param[in] filter the goertzel filter data.
 * @param[in] result the result of the goertzel filter data.
 */
static void goertzel_callback(struct goertzel_data_t *filter, float result)
{
    goertzel_data_t *filt = (goertzel_data_t *)filter;
    float logVal = 10.0f * log10f(result);
    // ESP_LOGI(TAG, "callback %d hz %.2f amp",filt->target_frequency,logVal);
    // Detection filter. Only above 25 dB(A)
    if (logVal > GROETZEL_MIN_DB)
    {
        // ESP_LOGI(TAG, "[Goertzel] Callback Freq: %d Hz amplitude: %.2f", filt->target_frequency, logVal);

        // call callback function
        if (on_goertzel_callback != NULL)
        {
            goertzel_free(goertzel_filt);
            (*on_goertzel_callback)();
        }
    }
}

void frequency_analysis_set_on_goertzel_callback(void (*callback)(void))
{
    on_goertzel_callback = callback;
}

void frequency_analysis_set_on_stop_callback(void (*callback)(void))
{
    on_listening_stopped_callback = callback;
}

/**
 * @brief creates a pipeline that will be used for the recording and sampling
 */
void create_pipeline()
{
    // ESP_LOGI(TAG, "Creating audio pipeline for recording");
    audio_pipeline_cfg_t pipeline_cfg = DEFAULT_AUDIO_PIPELINE_CONFIG();
    pipeline = audio_pipeline_init(&pipeline_cfg);
    mem_assert(pipeline);
}

/**
 * @brief creates a sample reducing filter to use in the goertzel algorithm
 */
void create_sample_reducing_filter(audio_element_handle_t *filter)
{
    // Filter for reducing sample rate
    // ESP_LOGI(TAG, "Creating filter to resample audio data");
    rsp_filter_cfg_t rsp_cfg = DEFAULT_RESAMPLE_FILTER_CONFIG();
    rsp_cfg.src_rate = AUDIO_SAMPLE_RATE;
    rsp_cfg.src_ch = 2;
    rsp_cfg.dest_rate = GOERTZEL_SAMPLE_RATE_HZ;
    rsp_cfg.dest_ch = 1;
    *filter = rsp_filter_init(&rsp_cfg);
}

/**
 * @brief creates a raw to read from when recording
 */
void create_raw(audio_element_handle_t *raw_read)
{
    // ESP_LOGI(TAG, "Creating raw to receive data");
    raw_stream_cfg_t raw_cfg = {
        .out_rb_size = 8 * 1024,
        .type = AUDIO_STREAM_READER,
    };
    *raw_read = raw_stream_init(&raw_cfg);
}

/**
 * @brief creates an i2s stream to use for recording
 */
void create_i2s_stream()
{
    // ESP_LOGI(TAG, "Creating i2s stream to read audio data from codec chip");
    i2s_stream_cfg_t i2s_cfg = I2S_STREAM_CFG_DEFAULT();
    i2s_cfg.i2s_config.sample_rate = AUDIO_SAMPLE_RATE;
    i2s_cfg.type = AUDIO_STREAM_READER;
    i2s_stream_reader = i2s_stream_init(&i2s_cfg);
}

/**
 * @brief registers all elements to the pipeline
 */
void register_elements()
{
    // ESP_LOGI(TAG, "Registering all elements to audio pipeline");
    audio_pipeline_register(pipeline, i2s_stream_reader, "i2s");
    audio_pipeline_register(pipeline, filter, "filter");
    audio_pipeline_register(pipeline, raw_read, "raw");
}

/**
 * @brief sets up the goertzel algorithm elements.
 */
void setup_goertzel()
{
    // ESP_LOGI(TAG, "setting up goertzel");
    // Config goertzel filters
    goertzel_filt = goertzel_malloc(GOERTZEL_N_DETECTION); // Alloc mem
    // Apply configuration for all filters
    for (int i = 0; i < GOERTZEL_N_DETECTION; i++)
    {
        goertzel_data_t *currFilter = goertzel_filt[i];
        currFilter->samples = GOERTZEL_BUFFER_LENGTH;
        currFilter->sample_rate = GOERTZEL_SAMPLE_RATE_HZ;
        currFilter->target_frequency = GOERTZEL_DETECT_FREQUENCIES[i];
        currFilter->goertzel_cb = &goertzel_callback;
    }

    // Initialize goertzel filters
    goertzel_init_configs(goertzel_filt, GOERTZEL_N_DETECTION);
}

void frequency_analysis_stop()
{
    loop = false;
}

/**
 * @brief cleans and deinits the pipeline.
 * 
 * @param p the pipeline handle
 */
void frequency_analysis_cleanup(audio_pipeline_handle_t p)
{

    // ESP_LOGI(TAG, "cleaning up...");
    if (raw_buff != NULL)
    {
        free(raw_buff);
        raw_buff = NULL;
    }

    goertzel_free(goertzel_filt);

    audio_pipeline_terminate(p);

    audio_pipeline_remove_listener(p);

    audio_pipeline_unregister(p, i2s_stream_reader);
    audio_pipeline_unregister(p, filter);
    audio_pipeline_unregister(p, raw_read);

    audio_pipeline_deinit(p);
    audio_element_deinit(i2s_stream_reader);
    audio_element_deinit(filter);
    audio_element_deinit(raw_read);
    // ESP_LOGI(TAG, "done cleaning up");
}

/**
 * @brief sets up the sample filter and the raw reading stream for processing with the goertzel algoritm. 
 */
void frequency_analysis_init()
{
    // ESP_LOGI(TAG, "init frequency analysis");

    create_pipeline();
    create_i2s_stream();

    create_sample_reducing_filter(&filter);
    create_raw(&raw_read);

    register_elements();

    ESP_LOGI(TAG, "Linking elements together [codec_chip]-->i2s_stream-->filter-->raw-->[Goertzel]");
    audio_pipeline_link(pipeline, (const char *[]){"i2s", "filter", "raw"}, 3);

    setup_goertzel();

    // ESP_LOGI(TAG, "Starting audio_pipeline");
    audio_pipeline_run(pipeline);

    loop = true;
    raw_buff = (int16_t *)malloc(GOERTZEL_BUFFER_LENGTH * sizeof(short));
    if (raw_buff == NULL)
    {
        ESP_LOGE(TAG, "[TASK] Memory allocation for raw read bufferfailed!");
        loop = false;
    }
}

/**
 * @brief the task that gets called when starting listening
 * 
 * @param pvParameters the task parameter
 */
void goertzel_task(void *pvParameters)
{
    // ESP_LOGI(TAG, "[TASK] starting task");
    audio_pipeline_handle_t pl = (audio_pipeline_handle_t)pvParameters;

    if (pl == NULL)
    {
        ESP_LOGE(TAG, "[TASK] pipeline handle pointer was null!");
        loop = false;
        vTaskDelete(NULL);
    }

    while (loop)
    {
        if (raw_read == NULL || raw_buff == NULL)
            ESP_LOGE(TAG, "raws are null!");

        raw_stream_read(raw_read, (char *)raw_buff, GOERTZEL_BUFFER_LENGTH * sizeof(short));

        if (goertzel_filt != NULL)
        {
            goertzel_proces(goertzel_filt, GOERTZEL_N_DETECTION, raw_buff, GOERTZEL_BUFFER_LENGTH);
        }
    }
    // ESP_LOGI(TAG, "[TASK] stopped loop");

    frequency_analysis_cleanup(pl);

    if (on_listening_stopped_callback != NULL)
        (*on_listening_stopped_callback)();

    // ESP_LOGI(TAG, "[TASK] deleting task...");
    vTaskDelete(NULL);
}

void frequency_analysis_start_listening()
{
    // ESP_LOGI(TAG, "starting listening");
    frequency_analysis_init();
    xTaskCreate(&goertzel_task, "goertzel-task", 2048, pipeline, 5, NULL);
}